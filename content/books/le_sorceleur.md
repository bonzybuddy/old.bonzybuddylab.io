---
title: "Le Sorceleur (The Witcher)"
date: 2021-01-27T17:32:05+01:00
draft: false
author: "Andrzej Sapkowski"
genres: 
- Fantaisie
- Roman
release_year: "1986-2013"
---
## 🗣 De quoi ça parle

L'histoire se passe dans un monde médiéval avec des rois, des reines, des magiciens mais aussi des créatures non humaines comme des dragons, des elfes ou des nains. Les humains ont le pouvoir sur le monde et ils persécutent assez ouvertement toutes les autres créatures. Un des métiers de cette époque est celui de "sorceleur" un mutant mi-humain chargé de tuer toutes les créatures nuisibles pour les humains et la société. Le héros de l'histoire est donc un sorceleur du nom de Geralt qui part à la recherche de monstres à tuer pour gagner sa vie. C'est un redoutable combattant à l'épée. Il terrasse tous ses ennemis de par son agilité et sa rapidité d'esprit. Il est également contraint de protéger une petite fille qui est la descendante du trône d'un royaume envahi. Ses propres histoires de coeur avec une magicienne le troublent aussi. La guerre est sur le point d'éclater une nouvelle fois avec le pays voisin et l'espionnage s'en mêle afin de recueillir des informations sur l'ennemi. Geralt et ses amis doivent contrer les attaques sournoises afin de protéger leurs vies et leur pays. De nouveaux personnages font leur apparition au fil des tomes et l'histoire de Geralt et de la petite fille, Ciri, se complique et prend une ampleur considérable.

## 🔎 Comment je l'ai découvert

Une recommandation de Gaël, un pote

## 💭 Ce que j'en ai pensé

Je pense qu'il s'agit d'une critique de notre civilisation moderne décrite à travers une fiction d'une autre époque. De nombreux passages et dialogues sont appropriés à des situations de conflits actuels. Ces réflexions un peu masquées traitent du pouvoir, de la gloire et des inégalités. Tous ceux qui ne sont pas 100% humains sont mal vus et/ou considérés comme hostiles. Pourtant ils ont des diversités intéressantes et chacun pourrait avoir sa place dans la société. Le monde qui en résulte semble un peu perdu. Le pouvoir est détenu entre les mains de certaines personnes uniquement, avides d'étendre leurs territoires et leurs influences. Des guerres éclatent, des trahisons s'entremêlent. Au centre de ce monde et de cette histoire se trouve notre sorceleur, lui aussi un marginal qui cherche sa place et est un peu sombre d'esprit. Le sorceleur est présenté comme une sorte de monstre créé pour tuer les autres monstres, nocifs pour les humains. Pourtant il est perçu lui aussi comme un être extraordinaire et rejeté. Il dit ne pas ressentir les sentiments pourtant cela ne semble pas être le cas.

## 👏 Ce que j'ai aimé

Le héros est puissant et plutôt attachant même si son caractère têtu est très marqué. Les autres personnages sont aussi plutôt attachants. On apprend à les apprécier, l'histoire est relativement bien développée autour de chacun, il y a du suspens, des choses qui sont dites au fur et à mesure du livre donc on a envie de continuer à lire. Les combats sont nombreux et bien écrits. Ils tiennent en haleine. Le sorceleur voyage beaucoup dans ce monde imaginaire, il rencontre de nombreuses personnes/monstres donc ce n'est pas rébarbatif. Il y a pleins de petites histoires dans l'histoire principale. A chaque petite histoire on apprend des choses sur le passé et le but du héros dans ce monde. On retrouve les personnages au fur et à mesure.

## 👎 Ce que je n'ai pas aimé

J'ai l'impression que ce n'est pas toujours très bien écrit ou alors il s'agit d'un problème de traduction. 

D'autre part, les scènes de combats sont assez violentes et (trop) bien décrites, ce qui est un peu trop réaliste à mon goût.

Ce que j'ai trouvé un peu ennuyant par moment ce sont les longs passages qui se déroulent sans l'interaction des personnages principaux entre eux, il faut s'accrocher pour continuer.

La fin n'est pas à la hauteur de mes espérances et il y a beaucoup de questions qui sont laissées sans réponse.

## 🎯 Qui l'apprécierait

Les gens qui aiment les histoires de monstres et l'univers médiéval des chevaliers, sorciers, magiciens et autres créatures un peu fantastiques. 

Aussi ceux qui n'ont pas peur de se lancer dans une longue série (+ de 3000 pages).

Egalement ceux qui aiment connaître l'origine des jeux vidéos ou series à succès car souvent il s'agit de livre à la base.