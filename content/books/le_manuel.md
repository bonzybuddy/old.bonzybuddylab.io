---
title: "Le Manuel"
date: 2021-02-07T12:29:45+01:00
draft: false
author: "Epictète (trad. André Dacier)"
genres: 
- Philosophie
release_year: ~125
---

## 🎣 Impressions

Un petit livre facile et rapide à lire, composé de 83 passages allant d'une ligne à une page.

Il permet d'appronfondir le Stoïcisme, malgré des désaccords sur plusieurs points/passages.

## 🔎 Comment je l'ai découvert

Un classique de la pensée Stoïque, ceux qui s'y intéressent doivent tôt ou tard le lire.

## 🎯 Qui devrait le lire

Comme dit plus haut, tous ceux qui s'intéressent aux grands noms de la philosophie Stoïque, ceux qui aiment les livres (très) courts, ceux qui aiment le livres sous forme de mini chapitres indépendants.

## 🎉 Ce que que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- Quelques passages m'ont pronfondément marqué notamment au début du livre. Ce sont aussi les plus courts. Ces passages m'ont fait évoluer dans ma recherche "d'homme sage" au sens Stoïque.
- Il m'a permis de mieux comprendre certains aspects du Stoïcisme et d'avoir le point de vue d'un auteur de l'époque, de soulever des points de divergence entre la pensée de l'auteur et la mienne.

## 💬 Mes 2 citations préférées

> De toutes les choses du monde, les unes dépendent de nous, les autres n'en dépendent pas. (I)

> Ce qui trouble les hommes, ce ne sont pas les choses, mais les opinions qu'ils en ont. (X)

## 📝 Résumé + notes personnelles

Je n'ai retenu que quelques passages sur les 83 composant ce "Manuel". Ce sont ceux qui m'ont apporté le plus de changements dans ma manière de penser et ceux avec lesquels j'étais le plus en accord.

> Si tu aimes ton fils ou ta femme, dis-toi à toi même que tu aimes un être mortel ; et s'il vient à mourir, tu n'en seras point troublé.

Cette citation du passage VIII peut paraître troublante à première vue mais elle est au final très représentative du mode de pensée Stoïque qui veut que l'on soit détaché des choses qui ne sont pas sous notre contrôle. Ainsi, si l'on accepte le fait qu'il est impossible d'agir sur certaines choses, les changements brutaux ou inattendus seront mieux vécus. Ici Epictète prend l'exemple de la mort d'un proche. Il n'est pas en notre pouvoir de contrôler la vie donc nous ne devrions pas être affligés par ce type d'événement. Il ne s'agit pas d'être dénué d'émotion et d'être traité de "sans coeur" mais bien de laisser passer l'émotion sans qu'elle nous submerge ou nous détruise. Il renchérit dans le passage XXI où il traite de fou n'importe quel Homme désirant que ses proches vivent pour toujours. En essayant de contrôler ce qui ne dépend pas directement de nous, on devient frustré et malheureux.

(X) Pour comprendre cela nous avons besoin d'un autre grand principe de la philosophie Stoïque : **Ce que nous percevons comme étant mauvais n'est mauvais que car nous l'avons décidé ainsi**.

En raisonnant de cette manière on est capable d'accepter beaucoup plus d'actions. (XXX) Ce n'est pas celui qui nous injure ou qui nous fait du mal qui nous irrite mais l'opinion que l'on a de la personne. C'est pourquoi, lorsque ce genre d'événement survient il faut raisonner de manière logique et ne pas se laisser emporter.

(XVII) Plutôt que de se dire *j'ai perdu telle chose,* dites plutôt *je l'ai rendu.* Cette manière de raisonner est présente dans les religions monothéistes lorsque l'on dit, "il est mort, Dieu l'a repris à ses côtés".

(XI et XIV) L'homme sage est quelqu'un qui n'accuse ni les autres ni soi-même de ses malheurs. C'est aussi quelqu'un qui ne cherche pas à changer la manière dont les choses arrivent mais qui, au contraire, les accepte telles quelles. Cet homme sage aspire à la quiétude et à la tranquilité de l'âme (*ataraxie).* Pour cela il se débarasse de ses soucis et de ses craintes.

(XIX et XXXI) Pour avancer dans la sagesse, il ne faut pas non plus avoir peur de paraître ignorant et imbécile sur un sujet. Epictète prend l'exemple d'une personne souhaitant devenir philosophe à son époque. Il était d'abord hué et insulté d'arrogant. Mais si cette personne maintient ses positions, s'affirme et ne recule pas devant les moqueries elle sera ensuite admirée. Ceci est encore d'actualité de nos jours. On a souvent peur de se lancer dans quelque chose qui nous confronte aux autres.

(XV) Les blessures physiques ne consitutent pas un frein à notre détermination. Selon Epictète, l'esprit est plus fort que le corps. Rien ne devrait empêcher notre volonté d'accomplir.

(XXVIII) Afin d'être libre (au sens psychologique) il faut se contenter d'apprécier les choses que l'on contrôle et oublier le reste.

(XXXV) Les conseils de Stoïcisme donnés aux autres doivent être également appliqués par celui qui les donne. En préconisant de ne pas être alteré par tel ou tel événement lorsqu'il survient pour un ami, il faut se garder de réagir comme lui si ce même événement nous arrive.

(L) L'homme sage est un homme mesuré qui ne tombe pas dans l'excès. Ici Epictète préconise d'utiliser avec parcimonie "la nourriture, la boisson, le logement ou les domestiques et de rejeter la mollesse et la vanité."

(LVI) On retrouve dans ce passage deux principes Stoïques : la préméditation de l'adversité qui stipule que l'on doit être préparé à tout type d'événements et la concentration sur les choses qui ne dépendent que de nous.

(LXXI) Ne pas juger les actions des autres en hâte sans savoir précisement ce qui les pousse à agir de la sorte. Souvent ces raisons nous sont cachées.

(LXXII) Au lieu de donner des leçons sur la manière de vivre et de paraître vaniteux, contente toi de le faire et les autres le remarqueront d'eux-mêmes.

(LXXVII) Le Stoïque cherche à être en accord avec la Nature. Pour cela il faut s'appuyer sur ceux qui l'expliquent de la meilleure manière possible. Et à partir de ces interprétations il met en pratique ce qu'il a appris. L'important n'est pas qui le dit mais ce qu'il dit.