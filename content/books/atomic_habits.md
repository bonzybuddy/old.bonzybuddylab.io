---
title: "Atomic Habits"
date: 2021-01-23T17:32:12+01:00 
draft: false
author: "James Clear"
genres:
  - Productivité
  - Life Advice
release_year: "2018"
cover: 
  img: /covers/atomic-habits-cover.png
  source: https://jamesclear.com/atomic-habits 
---

## 🥇🥈🥉 Le livre en 3 phrases

1. De petits changements mènent à de grands résultats
2. Les habitudes peuvent transformer notre vie pour nous aider à atteindre nos objectifs mais peuvent aussi nous détruire
3. En plus des habitudes, l'amélioration continue est nécessaire sous peine de régression

## 🎣 Impressions

Le livre est facile à lire et à comprendre, bien documenté et bien structuré : un exemple à chaque début de chapitre, un développement de l'idée principale et un petit résumé en fin de chapitre. On ne se perd donc pas à la lecture et on peut revenir facilement sur une notion grâce à l'index en fin de livre. C'est un livre qui donne de nombreux conseils pour prendre de bonnes habitudes et pour éviter les mauvaises. Le livre fonctionne ainsi dans les 2 sens ce qui est pratique. De plus, il est agrémenté de figures et schémas simples et compréhensifs et d'un tableau récapitulatif de chaque partie et sous partie.

## 🔎 Comment je l'ai découvert

Ali Abdaal recommande ce livre.

## 🎯 Qui devrait le lire

Tous ceux qui souhaitent prendre de bonnes habitudes (et qui n'y arrivent pas) ou bien ceux qui veulent se débarasser de leurs mauvaises habitudes.
Un peu pour ceux qui s'intéressent à la psychologie et au fonctionnement du cerveau dans certains cas de figures comme le désir ou la récompense.

## 🎉️ Ce que que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- Les approches menées par l'auteur sur _pourquoi_ et _comment_ prendre de bonnes habitudes et se débarrasser des mauvaises sont intéressantes. Elles permettent de mettre des mots sur des concepts que je connaissais ou que j'appliquais mais de manière uniquement intuitive.
- Nombre de ses méthodes sont reprises dans les vidéos et les conseils d'Ali Abdaal et font donc partie des conseils que j'applique dorénavant moi-même. Par exemple, l'environnement design est une chose que j'ai implémenté afin de m'immerger plus facilement dans mon travail.
- J'ai aussi pris conscience de l'importance de la persévérance et malgré un temps assez long entre le début et les premiers résultats, ça en vaut toujours la peine.
- Ne plus avoir peur des grands projets qu'il suffit de diviser en petits morceaux qui sont améliorés au fur et à mesure.

## 💬 Mes 5 citations préférées

> Too often, we convince ourselves that massive success requires massive action.

> If you want to master a habit, the key is to start with repetition, not perfection

> Commitment device is a choice you make in the present that controls your actions in the future.

> Never miss twice

> When you can't win by being better, you can win by being different

## 📝 Résumé + notes personnelles

Dans le premier chapitre de la première partie intitulée : _pourquoi de petites améliorations mènent à de grands résultats_, James Clear mentionne l'entraîneur de l'ex-équipe cycliste britannique Sky, Dave Brailsford, qui fonctionne selon le principe d'accumulation de petites améliorations à la base jugées insignifiantes. Il divise chaque étape d'entraînement d'un cycliste puis les améliore une par une afin d'avoir une grosse plus value finale.

_"Trop souvent, nous nous convainquons nous-mêmes qu'un grand succès provient d'une grande action."_ La dernière partie de la phrase est effectivement fausse car on ne peut pas s'améliorer instantanemment du jour au lendemain, cette étape d'amélioration requiert un temps et une persévérance. La théorie du _1% better every day_ nous dit qu'en s'améliorant d'1% par jour on est 37% plus performant au bout d'un an, alors que sans progression on régresse à 0.

<center>

__0.99<sup>365</sup> = 0.03__

__1.01<sup>365</sup> = 37.78__
</center>

Ce que nous sommes provient de nos habitudes, elles reflètent notre mode de vie et notre personnalité. Je suis ce que je mange, je sais ce que j'apprends... Nous sommes ce que nous répétons.
Nos habitudes reflètent donc notre identité et la persone que nous voulons être. C'est pourquoi il faut être capable de modifer son identité de manière a faire correspondre celle-ci avec nos habitudes. Exemple : je veux me mettre à courir, je dois d'abord modifier mon identité (c'est-à-dire ce que je pense de moi même) pour me voir comme une personne sportive qui court. Notre identité est donc flexible et s'adapte régulièrement, mais il faut être prêt à vouloir la changer. D'ailleurs le mot identité provient du latin *essentitas* signifiant **être** et *identidem* qui signifie **de manière répétée**. Notre identité est donc composée de ce que nous répétons.
Les 4 étapes d'une habitude peuvent être décrites comme étant la suite des événements cue-craving-response-reward que l'on peut traduire par signal, envie, action, récompense.

L'effet Diderot est le fait qu'un achat en entraîne un autre, ce qui crée une spirale de consommation car chaque achat se suit d'un nouveau.

Une histoire étonnante est celle de Laszlo Polgar, un Hongrois qui a voulu expérimenter si les gênes sont impliqués dans la réussite. Il eut 3 filles et a basé toute leur éducation et leur vie sur le jeu des échecs. Ainsi, les 3 filles devinrent passionnées d'échecs et championnes mondiales. Ceci grâce à leur pratique assidue mais surtout par rapport au fait que les échecs faisaient partie intégrante de leur vie. Elles n'en étaient pas moins heureuses et adoraient leurs vies. Je trouve cette histoire incroyable car elle montre que l'on peut devenir un grand maître dans un domaine sans pour autant avoir les gênes du succès et avoir une vie "normale".

Nous avons tendance à imiter les habitudes de trois groupes de personnes : les proches, la majorité et les puissants.
Une des choses les plus utiles pour prendre de bonnes habitudes est de s'entourer de gens qui partagent la même passion que nous afin que celle-ci paraisse tout à fait normale. Si je m'entoure de personnes sportives je serai plus facilement amené à faire du sport que si mes amis sont sédentaires.

Lorsque l'on vit en société comme c'est notre cas, il est facile de se plier aux habitudes du groupe car c'est ce qui est décrit comme étant la norme. Ainsi nous préférons avoir tort avec le groupe plutôt que d'avoir raison tout seul car dans le cas contraire un sentiment de rejet peut être ressenti. On peut par contre ne pas prêter attention aux regards des autres et continuer selon son propre chemin et ses propres habitudes jugées plus pertinentes mais cela demande un effort supplémentaire et éventuellement une pression.

Nos habitudes modernes proviennent en fait de nos désirs primitifs. Par exemple, pour trouver l'amour et se reproduire on utilise aujourd'hui Tinder mais le principe est le même. Pour gagner de la reconnaissance et de l'estime de soi on poste sur Instagram. Pour se connecter aux autres et communiquer on utilise Twitter ou Facebook. Toutes ces applications modernes ne sont que des réponses aux besoins primitifs de l'homme.

L'auteur aborde ensuite une comparaison que je trouve judicieuse. Il compare les mots anglais *motion* et *action* qui signifient **mouvement** et **action** en français. Bien qu'ils paraissent synonymes il y a une différence subtile. Lorsque l'on fait preuve de **mouvement** on accumule des connaissances et on planifie mais on est passif. Tandis que lorsque l'on est en **action** on réalise vraiment quelque chose ; on est actif. Le problème avec le **mouvement**, c'est qu'on exécute aucune tâche, autrement dit on ne progresse pas. Parfois le **mouvement** est utile mais il ne sera jamais liée à un changement véritable. L'auteur prend l'exemple de parler à son coach personnel, on apprend des choses, on fait une démarche de lui parler mais aucun résultat n'apparaît sans mise en **action**.

Il relie ensuite ces deux notions au fait que nous ne voulons pas être critiqués pour nos **actions** et notre travail donc on tend souvent à de pas produire d'**action.** On cherche alors à retarder le moment où l'on produit quelque chose pour retarder le moment de la critique.

Une des clés des habitudes est de commencer par répéter une action afin qu'elle devienne une habitude. Peu importe si elle est imparfaite au début, le principal est de commencer et elle sera peaufinée par la suite. Nombre de personnes ne veulent pas commencer telle ou telle habitude car ils ont peur de ne pas le faire correctement. Résultat : il ne commencent jamais et n'évoluent jamais vers l'identité qu'ils souhaiteraient atteindre. On ne peut pas améliorer une habitude qui n'existe pas.

De plus, ce qui compte réellement dans notre changement de comportement c'est la fréquence à laquelle on exécute une habitude, pas le temps. Cela signifie que l'on peut s'entraîner pendant 10 ans à apprendre un instrument mais que si l'on pratique uniquement 1 fois par semaine on ne progressera que très peu voire pas su tout, tandis que pratiquer tous les jours pendant 1 an sera beaucoup plus productif.

Nous passons ensuite à la loi du moindre effort. Moins l'habitude requiert d'énergie et plus on sera enclins à l'adopter. Une des manières de réduire cette énergie est de pratiquer l'*environnement design* autrement dit de faire en sorte qu'un lieu soit adapté à une tâche. Par exemple un bureau clair et organisé nous incite à travailler, des outils rangés nous incitent à bricoler, une guitare posée à côté du canapé nous incite à en jouer etc. Donc moins on met d'effort pour nous préparer à faire une chose et plus facile il sera de la faire. Si je n'ai qu'à tendre le bras pour attraper ma guitare et en jouer je le ferai plus souvent que si elle est rangée dans sa housse dans un placard au grenier.

*Commitment device is a choice you make in the present that controls your actions in the future.* Il y a pleins d'exemples de commitment device que j'adore. Cela rejoint la théorie Measure de Taimur qui préconise d'investir dans quelque chose qui change notre vie en mieux (voir le podcast *Not Otherthinking* episode 5: Kitchen Bin). Ici ce qui est intéressant c'est de comprendre que certains petits choix peuvent radicalement changer nos habitudes. Enlever la télé de notre chambre par exemple. Désinstaller les appli de réseaux sociaux sur notre téléphone. Je pratiquais déjà la plupart de ces idées mais j'apprécie n'être pas le seul a y avoir pensé.

Dans le chapitre sur comment être consistent, l'auteur propose une solution assez simple mais suffisamment efficace : ne pas manquer plus de deux fois d'affilée notre habitude. Par exemple, si l'on décide de prendre l'habitude de méditer tous les matins (comme c'est dorénavant mon cas) on peut y manquer une fois pour telle ou telle raison, mais pas deux matins de suite. Car sinon on commence à prendre une mauvaise habitude de ne pas le faire et un matin raté suivi d'un autre et encore un autre mène à ne jamais le faire.

Afin de trouver les domaines qui nous intéressent et de les approndir on utilise la méthode explore/exploit trade-off. Autrement dit on essaie un maximum de possibilités pour avoir un large panel de choses existantes et de pouvoir se faire une idée de ce qui nous attire ou pas.

La différence entre un professionnel et un amateur est la discipline. Au bout d'un certain temps une habitude peut devenir redondante et lassante ou douloureuse. Là où le novice voudra arrêter, le professionnel s'obligera à continuer car il en relève de son domaine et de son métier.

