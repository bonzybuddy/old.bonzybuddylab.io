---
title: "Comment améliorer son alimentation"
date: 2021-02-06T13:08:06+01:00
# maj: "08-02-2021"
draft: false
author: "ilone"
---

_Pour ceux qui souhaitent davantage de détails je vous invite à consulter directement le blog [www.sante-et-nutrition.com](http://www.sante-et-nutrition.com/) (toutes les images incluses dans ce résumé sont la propriété d'Anthony Berthou)_

Après la lecture du livre _La Clinique du Coureur_ j'ai bien pris
conscience de l'importance de l'alimentation dans le sport mais aussi
dans la vie quotidienne. Parmi les spécialistes interrogés, Anthony
Berthou, nutritionniste spécialisé en nutrition sportive proposait son
blog sur le sujet. Après avoir consulté la quasi-totalité des articles
(qui sont par ailleurs très complets), je me suis dit qu'il serait très
intéressant d'en faire un résumé pour souligner l'importance de la
nutrition et pour ceux qui n'ont pas le temps de lire autant de
ressources.

Voici donc mon résumé :

**La mastication et le respect de la satiété est la première règle à respecter !**

Les autres règles sont :

1.  équilibre OM3/OM6
2.  équilibre acido-basique
3.  antioxydants
4.  diminution des glucides
5.  intégrité de l'écosystème intestinal

## Equilibre Oméga 3/Oméga 6

- privilégier les oeufs Bleu Blanc Coeur (BBC) et/ou catégorie 0 (bio)
  (Oeuf à la coque (avec le jaune coulant et le blanc cuit) > oeuf au plat)
- 2 cuillères à soupe (c.a.s.) d'huile **vierge première pression à froid crue** de colza, noix ou cameline (à conserver au frigo 3
  mois max) + 2 c.a.s d'huile d'olive (à utiliser aussi pour la cuisson).
- 2 à 3x/semaine petits poissons gras (sardines, maquereaux, anchois)
  mieux que thon et saumon
- 30 à 60g d'oléagineux/jour (amandes, noix, noisettes et
  éventuellement purée d'amandes)
- au moins 1,7g d'OM3/jour

## Equilibre acido-basique

- réduire l'apport en sel (chlorure de sodium) donc réduire les produits
  industriels
- fruits et légumes en quantité
- cuisson à la vapeur
- consommer des aliments au PRAL négatif **(Potential Renal Acid Load)** (fruits et légumes)

 <a href="http://www.sante-et-nutrition.com/equilibre-acido-basique-sante/" target="_blank">
    <img class="img-fluid rounded mb-3" src="/covers/alim/tableau-PRAL.jpg" alt="Tableau PRAL">
</a>
 <a href="http://www.sante-et-nutrition.com/nutrition-environnement/" target="_blank">
<img class="img-fluid rounded mb-3" src="/covers/alim/fruits-legumes-saison.jpg" alt="Fruits et Légumes de saison">
</a>

## Antioxydants (polyphénols)

- fruits avec la peau
- baies
- chocolat noir
- épices et aromates (clou de girofle, curcuma + poivre, cannelle au
  top sinon thym, romarin, laurier, basilic, menthe, persil, cerfeuil,
  ciboulette, estragon, aneth)
- thé vert (pu-erh) ou blanc

Tous les thés sont riches en antioxydants (matcha particulièrement) mais le thé vert a bénéficié
de plus d'études et de conclusions bénéfiques à son égard. Le thé vert
est donc particulièrement riche en EGCG _(épigallocatéchine gallate)_.
Attention aux pesticides et aux métaux lourds dans les thés, car
même s'ils sont bio ils n'en sont pas forcément exempt. Se tourner
vers un passionné de thé pour choisir le bon. Infusion plus longue =
plus d'antioxydants et moins d'effets stimulants, à 85-90°C, en général 10mn d'infusion pour que la plupart soient extraits mais
cela est contraire aux recommandations des puristes par rapport au
goût. Rajouter quelques goutes de citron pour multiplier la quantité de
catéchines absorbées (jusqu'à 5 fois plus).

## Diminution des glucides

- éviter les pics de glycémie, notamment à jeûn ou en dehors des repas : consommation de fibres, de protéines, de lipides pour abaisser l'IG (Index Glycémique)
- privilégier les IG faibles (< 40), car le pic de glycémie est plus
  diffu et plus lent.
- les fruits ont un IG modéré
- miel foncé plutôt que clair (IG plus faible)
- mastiquer (au moins 20mn par repas)
- consommer des végétaux
- consommer des aliments lacto-fermentés (choucroute, kéfir, pain au
  levain)
- curcuma
- thé vert
- fibres à chaque repas pour réduire l'IG

<a href="http://www.sante-et-nutrition.com/index-glycemique/" target="_blank">
  <img class="img-fluid rounded" src="/covers/alim/IG.jpg" alt="Tableau IG">
  </a>

## Vitamine D

- 15-30mn/j d'exposition au soleil sans crème solaire du printemps à l'automne permet
d'assurer ses besoins. L'exposition estivale ne suffit pas à
constituer des réserves pour l'hiver. 5000UI/j nécessaires quotidiennement (1 UI = 0.025µg). Pour un
complément alimentaire on propose 75UI/kg/j dans la limite de 6000UI/j
pour un adulte. Mais une hypersupplémentation n'apparaît qu'à partir de 10 000UI/j.
- vitamine D3 (et pas D2) sous forme huileuse et pas sous forme de foie
de poisson en raison de la vitamine A bloquant l'effet de la D.

## Autres petits trucs

- le cacao du chocolat contient de la tryptophane qui
  permet de sécréter l'hormone de la zenitude et préparer
  l'organisme au sommeil (il est néanmoins très calorique)
- tremper les légumineuses avant cuisson pour limiter les
  facteurs anutritionnels
- éviter les acides gras trans et saturés (produits
  industriels), privilégier les acides gras monoinsaturés ou
  polyinsaturés (bonnes graisses : OM3 petits poissons oléagineux et
  OM9 que l'on trouve dans l'avocat et l'olive)
- pas de repas sans légume (idéalement un cru avant et un cuit
  pendant)
- brocolis cuit à la vapeur croquant et frais accompagné de wasabi
  et d'huile de colza
- poivre en petite quantité mais à associer avec le curcuma
  (multiplie par 1000 les effets)
- besoin en protéines entre 1 et 1,2g/kg de poids corporel/jour
  (si sport entre 1,3 et 2g/kg/j)
- le rapport protéines animales/végétales idéal est de 1
- diminuer la consommation de corps de Maillard qui
  apparaissent quand les aliments sont brunis, grillés (à
  haute température) et limitent l'assimilation des
  protéines.

## Petit déjeuner

- 2 oeufs à la coque cat 0 et/ou BBC
- 1 portion de 150g de fruit
- 30-50g d'oléagineux (noix, amandes, noisettes)
- 1 c.a.f. de purée d'amandes complètes (Jean Hervé)
- 1/4 de c.a.c. cannelle en poudre (antioxydant + régule IG)
- thé vert (ou blanc) infusé à 85°C ou moins
- 1 tranche max de pain de qualité
- (20-30g de flocons de céréales --avoine, petit épeautre,
  sarrasin-- *on peut néanmoins se passer totalement de céréales le matin)*

## Déjeuner

- *(entrée : salade, crudités avec épices, aromates, huile de qualité)*
- dominance protéines (volaille, poisson)
- légumes assaisonnés d'huile
- céréales (faim importante, entraînement matinal) ou légumineuses (lentilles, fèves, haricots secs, pois cassés, pois chiches etc.)
- *(dessert : choco noir + 20-30g d'oléagineux)*

## Collation de 16-17h

- 30g d'oléagineux (noix, amandes, noisettes)
- 1-2 carrés de chocolat noir
- *(1 fruit)*

## Dîner

- dominance végétale : 1/3 de légumes de saison, 1/3 de
  légumineuses et 1/3 de produits céréaliers. 
- sinon ½ légumes et ½ légumineuses ou ½ produits céréaliers complets. Considérer (selon
  votre appétit) environ 50-60g (poids cru) ou 150-200g (poids cuit)
  de légumineuses (lentilles, pois chiche, pois cassés, haricots
  rouges etc.), produits céréaliers et assimilés (riz complet,
  quinoa, sarrasin, patate douce, châtaigne, etc.)
- *(dessert : 1-2 carrés de chocolat noir + 1 fruit)*

## Récupération après course ou entraînement

- associer, tout de suite ou après 30min à 1h selon la
  sensibilité digestive, de petites quantités d'aliments solides
  consommées régulièrement dans l'attente du repas : banane bien mûre,
  compote, fruits oléagineux (noix, noisettes, amandes) selon la
  tolérance digestive, pâte de fruits, pâte d'amandes etc. Les buffets
  de ravitaillement contiennent souvent des fruits secs à consommer
  avec parcimonie en cas de fragilité digestive (de même pour le
  chocolat), du pain d'épices, des bananes, voire les pâtes
  (à éviter pour les personnes sensibles au gluten, de même pour le
  pain d'épices) ou du riz.
- au cours du repas de récupération : préférer des
  aliments glucidiques et alcalinisants comme les pommes de terre, les
  légumes et fruits bien mûrs voire pelés ou cuits en cas de
  sensibilité intestinale importante. Le riz est également adapté, de
  même que les légumineuses en fonction de la tolérance digestive. A
  l'inverse, éviter la consommation excessive de viande animale à
  l'origine d'une production accrue de déchets acidifiants. Préférer
  plutôt les préparations à base d'œufs (1 à 2), de poisson (environ
  100 à 150g) ou les protéines végétales et veiller à consommer des
  graisses crues de qualité (huile de colza, de lin, de cameline ou de
  noix première pression à froid en assaisonnement et des oléagineux).
  Veiller à bien saler les aliments, surtout si vous avez beaucoup
  transpiré pendant la course.
- continuer à grignoter des aliments glucidiques jusqu'au coucher
  et réitérer la prise de boisson de récupération.
- penser à intégrer des protéines un petit déjeuner du lendemain
  (2 œufs à la coque par exemple) et poursuivre une bonne hydratation au
  cours de la journée, tout en écoutant votre sensation de faim.

## CONCLUSION
<hr>
<a href="http://www.sante-et-nutrition.com/la-pyramide-alimentaire-de-la-nutrition-positive/" target="_blank">
    <img class="img-fluid rounded" src="/covers/alim/pyramide-alim.jpg" alt="Pyramide alimentaire">
</a>
