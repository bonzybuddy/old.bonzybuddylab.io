---
title: "The 4-Hour Workweek"
date: 2021-01-23T17:32:30+01:00
draft: false
author: "Timothy Ferriss"
genres:
  - Business
  - Créativité
  - Life Advice
  - Productivité
release_year: "2007"
cover: 
  img: /covers/the-4-hour-workweek-cover.jpeg
  source: https://tim.blog/tim-ferriss-books/
---

## 🥇🥈🥉 Le livre en 3 phrases

1.  Travailler (beaucoup) moins, gagner autant voire plus
2.  Faire partie des NR (New Rich) grâce aux étapes D.E.A.L.
3.  Vivre notre vie rêvée grâce au temps libre que l'on s'est créé

## 🎣 Impressions

J'ai trouvé le livre très fouilli dans la manière de présenter les
choses. L'idée principale est cernée autour de 4 chapitres majeurs mais
à l'intérieur de chaque chapitre il y a des mini-chapitres et des
témoignagnes ou des histoires racontées, on ne sait jamais trop qui
écrit quoi et quand.
Hormis ces détails, le contenu est relativement intéressant dans le sens
où il apporte une vision assez nouvelle du travail et de la vie. Ce
n'est pas le traditionnel schéma de pensée et j'aime ce côté
original. De nombreuses ressources sont données ce qui permet
d'appliquer assez fidèlement les principes évoqués mais comme le livre
date de 2007 (et réécrit en 2009), il est possible qu'elles ne soient
plus d'actualité.

## 🔎 Comment je l'ai découvert

Une des recommandations d'Ali Abdaal.

## 🎯 Qui devrait le lire

Tous ceux qui souhaitent changer leur mode de vie de manière profonde et
qui pensent que la vie se passe en dehors d'un bureau. Ceux qui ne
supportent pas d'avoir un boss et peu de temps libre.
Ceux qui veulent voyager, ceux qui veulent accomplir des choses en
dehors de leur travail, ce dernier ne servant qu'à leur apporter de
l'argent.

## 🎉️ Ce que que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- Je réfléchis maintenant à faire partie de cette nouvelle caste de NR
  afin de ne jamais -ou du moins pendant un court laps de temps- faire
  partie du monde du travail classique car ce n'est pas ce à quoi
  j'aspire. Cependant l'intérêt des NR par rapport aux gens qui
  décident de ne pas travailler dans un bureau est qu'ils disposent
  de plus d'argent. Ainsi un NR est suffisamment riche pour vivre
  plus que confortablement sans être un multi-millionnaire qui demande
  beaucoup d'implication dans son travail.
- Il faut d'abord trouver une idée d'entreprise à créer, appliquer
  étape par étape les conseils, rentrer dans ce monde afin de s'en
  imprégner, de voir ce qui fonctionne, ce qui ne fonctionne pas ou
  plus (car le livre date de 2007, le monde a bien changé depuis).
- L'idée générale est attirante et demande approfondissement. Le
  livre n'est qu'une approche de la chose mais suffisament probante
  pour savoir que cela fonctionne réellement et que ce n'est pas un
  coup de chance d'une personne en particulier ; de nombreuses
  personnes ont adopté ce lifestyle.

## 💬 Mes 2 citations préférées

> The NR are those who abandon the deferred-life plan and create luxury lifestyles in the present using the currency of the NR : time and mobility.

> Effectiveness is doing the things that get you closer to your goals.Efficiency is performing a given task (whether important or not) in the most economical manner possible. Being efficient without regard to effectiveness is the default mode of the universe.

## 📝 Résumé + notes personnelles

Le livre est divisé en 4 chapitres dans le but d'appliquer les 4 étapes
de la méthode DEAL (Definition, Elimination, Automation, Liberation)
afin de se libérer du monde du travail.

Les objectifs des NR (New Rich) sont d'avoir beaucoup de temps libre et
une grande mobilité. Ils ne sont pas contraints à rester travailler dans
un bureau, leur bureau est n'importe quel endroit dans le monde. Ce
temps libre leur sert à voyager, à passer du temps avec leur famille ou
leurs amis, faire du sport ou réaliser leurs passions. Pour atteindre
ces objectifs, ils appliquent la fameuse méthode DEAL afin de gagner
suffisamment d'argent sans trop travailler et être tranquilles dans
leur vie. Le but n'est pas de créer un business pour s'en occuper mais
de créer un business pour avoir une rentrée d'argent convenable sans
trop s'en occuper.

Voici un résumé des 4 chapitres.

**D pour Définition**

En première partie du livre, Tim Ferriss nous propose une définition de
ce nouveau mode de vie atypique et précise ce qu'est un NR et surtout
ce qu'il n'est pas. Un NR n'est pas un multimillionaire qui cherche à
faire beaucoup de profit pour gagner toujours plus d'argent ce qui
impliquerait de travailler toujours plus. Le but n'est pas d'être le
plus riche possible mais d'avoir le plus de temps libre possible.

Il nous incite aussi à ne pas suivre les recommandations universelles
comme celle de travailler toute sa vie pour pouvoir profiter de sa
retraite ou bien de travailler beaucoup d'heures car beaucoup de ce
temps est gâché.

Le lecteur est donc invité à plonger dans ce nouveau monde sans y avoir
peur. En effet, de nombreuses personnes voudraient changer leur vie mais
ont peur des conséquences désastreuses que cela pourrait apporter, ils
ont peur de ne pas réussir et de tout perdre. L'auteur nous aide à
relativiser le "tout perdre" car selon lui rien n'est perdu à jamais,
tout est réparable. Ainsi il nous invite à nous poser des questions sur
les pires scénarios possibles selon notre vie actuelle et à réfléchir
sur la véritable gravité des conséquences. Ces questions incluent, entre
autres, Quel serait le pire scénario possible ? Quelles sont les
possibilités de revenir en arrière ? Quels seraient les avantages d'une
nouvelle vie par rapport à l'ancienne ? Que feriez-vous si vous étiez
viré de votre job demain ? Quels sont nos peurs ? Quelles sont nos
attentes dans la vie ?

Avant de se lancer dans une nouvelle vie, celle des NR, il faut d'abord
se poser la question de ce que l'on recherche. De combien cela nous
coûtera, des rêves que nous voulons vivre, des choses que l'on souhaite
apprendre. Toutes ces grandes questions doivent être éclaircies avant de
commencer les étapes suivantes afin de savoir quelle est la finalité.

**E pour Elimination**

Sûrement ma partie préférée car elle est reliée au minimalisme. En
effet, afin d'éliminer le superflu dans notre travail il suffit
d'appliquer 2 règles bien connues des _productivity nerds_, la loi de
Parkinson et la loi 80/20 de Pareto.

La première stipule que le temps nécessaire pour accomplir une tache
dépend directement du temps alloué pour la réaliser. Autrement dit, pour
être le plus efficace il faut se donner des deadlines très faibles car
sinon la tache n'est jamais réalisée ou bien dans le rush juste avant
la deadline. Tandis qu'avec des fausses deadlines que l'on se crée
soi-même on devient beaucoup plus efficace. J'ai 2 semaines pour rendre
ce rapport ? Je me crée une deadline dans 4 jours ce qui me met une
pression suffisante pour fournir un travail de qualité en peu de temps.

La deuxième loi, 80/20 stipule que 80% de ce que nous produisons
provient de 20% des efforts. En clair, seule une petite portion de notre
travail constitue la majeur partie de celui-ci. Ainsi cela va nous
permettre de fournir peu de travail (seulement 20% de notre temps) afin
d'accomplir 80% des bénéfices ce qui est largement suffisant pour
obtenir de (très) bons résultats. Cela signifie aussi que pour obtenir
les 20% restants avant la perfection la quantité de travail à fournir
serait énorme et donc non compatible avec le mode de vie des NR. La loi
80/20 peut être utilisée dans les 2 sens : 80% des bénéfices proviennent
de 20% des efforts ET 80% des problèmes proviennent de 20% des sources.

Il y a une grande différence entre être efficace et être productif. Le
premier nous permet d'accomplir des choses utiles en adéquation avec
nos objectifs, le deuxième nous permet de faire beaucoup de choses sans
distinguer l'utile de l'inutile. Ainsi on peut être productif à
effectuer des taches toutes plus inutiles les unes que les autres sans
que cela ne nous apporte rien d'enrichissant. C'est exactement le
travail que l'on demande au robot, faire un maximum de taches dans un
laps de temps minimum. Or il ne faudrait pas passer du temps sur des
choses inutiles, il faudrait au contraire optimiser la productivité
uniquement des choses importantes qui nous permettent d'atteindre nos
objectifs. Encore une fois il s'agit de supprimer les choses
superflues.

Voici une phrase qui résume assez bien la chose : _"Slow down and
remember this: Most things make no difference. Being busy is a form of
laziness - lazy thinking and indiscriminate action. Being overwhelmed is
often as unproductive as doing nothing, and is far more unpleasant.
Being selective -doing less- is the path of the productive."_

Il conseille également de sélectionner ses sources d'information afin
de ne pas être submergé par celles-ci et de perdre le fil. Il propose
également une méthode pour lire beaucoup plus rapidement.

Il faut éliminer autant que possible les distractions afin de maximiser
sa productivité, voilà pourquoi le travail au bureau est aussi
chronophage, les sources de distractions sont nombreuses et ne
permettent pas de focaliser sur une tache afin de la finir rapidement.
Il propose donc de ne checker ses mails que 2x par jour (à 12h et à 16h)
afin de limiter les temps d'interruption induits par les mails. Idem
pour les appels ou les réunions. Lorsque l'on fait quelque chose il
faut être investi à fond dedans pour la faire correctement et vite.
Savoir dire non est aussi un art à maîtriser, car il nous permet de ne
pas diverger de nos objectifs et de notre tache tout en limitant le
temps perdu et les ambiguïtés.

**A pour Automatisation**

Afin de continuer à travailler peu mais en gagnant toujours autant voire
en faisant grossir son chiffre, il est important d'automatiser le
process car on ne peut pas gérer tout tout seul sinon notre travail
dévient chronophage et toute tentative de faire partie des NR est
réduite à néant.

Avant de déléguer il est important de se rappeler quelques règles. Ne
pas automatiser quelque chose qui peut être supprimé. Ne pas déléguer
quelque chose qui peut être automatisé.

Ainsi pour appliquer ce principe d'automatisation, il convient de
**déléguer** le travail. Pour cela, le plus simple consiste à faire
appel aux services d'un VA (Virtual Assistant) qui est 
une sorte de secrétaire attitrée qui fait les choses à notre place. Ce
VA a accès à toutes nos données et nos comptes (notamment mails) afin de
répondre à notre place dans des cas prédéfinis qui ne nécessitent pas
notre intervention ou bien qui nous transmettent les informations que
l'on juge utiles mais qui nous prendrait trop de temps à chercher
nous-mêmes.

Il faut trouver le domaine, la niche qui nous permettra de gagner de
l'argent. Rien ne sert de chercher à conquérir un marché déjà investi
largement par des grandes enseignes où faire notre place sera difficile.
Au contraire, chercher une niche, quitte à créer notre propre marché à
partir des passions que nous avons(exemple si je suis un pêcheur, je
suis apte à créer un produit dédié à la pêche car je connais les
problèmes qui pourraient être résolus, auquel cas d'autres pêcheurs
seraient intéressés par ce produit). Pour trouver cette niche, plusieurs
questions peuvent nous aider. A quel groupe ou catégories de personne
appartiens-je et qui pourrait être un domaine profitable ? Parmi ces
groupes, lesquels ont des magazines spécialisés dédiés ? (un peu
oldschool le magazine en 2021 mais vous avez l'idée principale). Quels
sont les problèmes que j'ai pu rencontrer et auxquels je peux apporter
une solution qui intéresserait d'autres personnes dans le même cas que
moi.

Pas la peine d'être un expert pour créer ou développer une entreprise,
un expert au sens marketing est une personne connaissant le sujet plus
que l'acheteur ce qui permet de relativiser l'importance du mot. Il en
faut souvent peu pour mieux connaître un sujet que la plupart des gens.
En lisant les 3 _bestsellers_ d'un sujet, je deviens un expert au yeux
de 80% des gens.

Il suffit de quelques clients qui dépensent un peu d'argent pour
obtenir le budget souhaité. Par exemple, pour la traversée de
l'Atlantique à la voile il faut un budget de 5000€. Si seulement 50
lecteurs d'un magazine spécialisé sur les 15000 sont convaincus par le
produit que je vends à 100€ j'ai obtenu mes 5000€ de budget
nécessaires.

Réaliser un micro test de produit avant de lancer une plus grande
production pour ne pas perdre d'argent à investir dans un projet qui
n'aboutira pas. Pour faire ce micro test, mettre de la pub afin de
quantifier le nombre de clients potentiels qui seraient intéressés (ceux
qui me contactent pour passer commande).

Une fois le produit et la clientèle créés, automatiser le process afin
de s'effacer du business. Supprimer les clients indésirables qui
causent plus de problèmes qu'ils ne rapportent. Laisser des libertés de
décision aux personnes qui gèrent l'entreprise à notre place (fixer une
somme limite en dessous de laquelle ils prennent eux-même une décision
sans avoir besoin de demander la permission d'agir). Faire quelque
chose de simple afin de diminuer les possibilités d'action et donc
d'erreurs et de temps perdu.

**L pour Libération**

Pour les gens qui ne souhaitent pas quitter leur job (ou bien petit à
petit sur une plus longue période suivant la réussite de sa propre
entrerprise) mais profiter quand-même du mode de vie des NR, il faut
réussir à faire du télétravail et démontrer que celui-ci est plus
efficace que le travail au bureau ce qui permet ensuite de négocier avec
son patron un mode de travail 100% à distance. Il faut par contre être
réellement plus productif chez soi qu'au bureau mais une fois prouvé,
plus de liberté sera accordée par notre supérieur.

Une fois que l'on a lancé son entreprise et fait suffisamment de
bénéfices pour avoir de l'argent (et quitter son ancien job par
exemple), que l'on a automatisé le process afin de s'effacer le plus
possible de son travail en déléguant, on peut enfin aspirer à la
libération c'est à dire disposer du temps libre nécessaire à
l'accomplissement de notre vie idéale. Si par exemple notre but est de
voyager à travers le monde en voilier, cela devient possible avec la
méthode DEAL. Nous nous sommes libérés du monde du travail classique. En
prenant des mini-retraites, c'est-à-dire des vacances prolongées on se
crée du bon temps pendant lequel on ne travaille presque plus ou le
moins possible.

Il ne faut par contre jamais checker ses mails lorsque ce n'est pas le
moment, au risque de tomber sur quelque chose de tracassant qui nous
minera le reste du WE ou des vacances.

Enfin le temps libre que nous nous sommes créé tout au long des
précédentes étapes doit nous permettre d'accomplir ce que nous
désirons le plus au monde. Pour cela, à chacun ses désirs et ses envies,
il n'y a pas de règles pour remplir son temps libre.
