---
title: "Le Prophète"
date: 2021-01-27T17:31:57+01:00
draft: false
author: "Khalil Gibran"
genres:
  - Philosophie
  - Life Advice
release_year: "1923"
---

## 🎣 Impressions

Très rapide à lire, très poétique, il permet de faire réfléchir sur la
sagesse en une soirée et de remettre en question des principes que l'on
pensait acquis et compris. C'est un livre court mais profond.

## 🔎 Comment je l'ai découvert

Mes parents me l'ont recommandé.

## 🎯 Qui devrait le lire

Tous ceux qui s'intéressent aux récits poétiques philosophiques et qui
cherchent des réponses à leurs questions existentielles.
Ceux qui aiment les livres courts.

## 🎉️ Ce que que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- Ce livre m'a apporté des éléments de réponse à certaines grandes
  questions. Il m'a appaisé et permis de voir les choses sous le
  prisme de la sagesse.
- Seulement certains thèmes m'ont touché mais cela suffit à changer
  ma manière de penser sur les Enfants, la Liberté, les Vêtements ou
  la Maison.
- Nous sommes les réponses à nos plus grandes questions, ceci peut
  paraître absurde de prime abord mais c'est ce que l'auteur suggère
  tout au long du livre. Toutes les réponses sont en nous, il suffit
  d'aller les chercher. Pour cela il faut instaurer un dialogue
  intérieur.

## 💬 Mes 3 citations préférées

> Vos enfants ne sont pas vos enfants. Ce sont les fils et les filles de la Vie qui se désire. Ils vous traversent mais ne sont pas de vous. Et s'ils vous entourent, ils ne sont pas à vous

> Ta douleur marque l'éclatement de la coquille qui enferme ta compréhension.

> Si l'enseignant est sage, il ne t'ordonnera pas d'entrer dans la demeure de sa sagesse, mais te conduira plutôt au seuil de ton propre esprit.

## 📝 Résumé + notes personnelles

Le livre est composé de 26 chapitres qui sont des développements courts
d'un thème général.

Ces thèmes sont l'Amour, le Mariage, les Enfants, Donner, la Nourriture
et la Boisson, le Travail, la Joie et la Peine, la Maison, les
Vêtements, Acheter et Vendre, Crime et Châtiment, les Lois, la Liberté,
la Raison et la Passion, la Douleur, la Connaissance de Soi,
l'Enseignement, l'Amitié, la Parole, le Temps, le Bien et le Mal, la
Prière, le Plaisir, la Beauté, la Religion et la Mort.

Il n'est pas vraiment intéressant ni possible de résumer chaque thème
car ils sont décrits en quelques pages et de manière concise.

Néanmoins on note l'omniprésence de dualités contraires qui ne forment
en réalité qu'une seule et même chose, par exemple le Bien et le Mal,
la Lumière et l'Ombre, la Vie et la Mort. Cette dualité est la même que
dans le Yin-Yang asiatique, et comme Bruce Lee le dit dans son livre
_Striking Thoughts_, cette dualité n'existe pas, c'est un tout
unique et indissociable. La culture occidentale aurait du mal à
comprendre ce tout et tente de diviser le concept.

De plus, un thème qui revient souvent est celui de la liberté, en
général sous-entendu mais bel et bien au coeur des développements.

Les thèmes qui m'ont le plus marqué ou plu sont les suivants (avec une
citation et un petit commentaire) :

**Le Mariage**

> Mais qu'il y ait de l'espace dans votre union

J'aime cette idée de liberté au sein même du mariage et de l'union car
ne nous devrions pas être complétement rattaché à l'autre mais garder
une certaine indépendance personnelle.

**Les Enfants**

> Vos enfants ne sont pas vos enfants. Ce sont les fils et les filles
> de la Vie qui se désire. Ils vous traversent mais ne sont pas de vous.
> Et s'ils vous entourent, ils ne sont pas à vous

> Tu t'efforces peut-être de leur ressembler, mais ne les oblige pas à
> te copier.

Cette idée que nos enfants ne nous appartiennent pas est assez
bouleversante et j'aime beaucoup le concept. Encore une fois cela
insinue une part de liberté individuelle, au-delà même des liens du
sang.

**La Maison**

> Construit toi d'abord une masure dans la solitude avant de te bâtir
> une maison entre les murs de la ville.

> En vérité, le désir de confort assassine l'âme passionnée
> avant de l'escorter, tout sourire, à son enterrement.

Avec ces mots l'auteur nous suggère de ne pas s'abandonner aux
plaisirs du confort car celui-ci nous emprisonne et nous affaiblit. La
liberté, vis-à-vis du détachement matériel, est encore une fois
évoquée.

**Vêtements**

> Et n'oublie pas que la terre s'enchante de sentir tes pieds nus,
> que le vent aspire à jouer dans tes cheveux.

Ceci est un appel à la libération des vêtements, marcher pieds nus et se
découvrir la tête permettent de ressentir au mieux la nature telle
qu'elle nous a créé. On retrouve le concept de marcher pieds nus
présent dans le livre _La Clinique du Coureur._

**Les Lois**

> Vous vous enchantez à édicter des lois, Et vous plaisez cependant
> davantage à les briser.

L'auteur décrit les lois humaines comme des cages que nous construisons
autour de nous et dont nous voulons finalement nous échapper ce qui est
paradoxal.

**La Liberté**

> Tu deviendras libre non quand tes jours seront dénués de souci, tes
> nuits de besoin et de douleur. Mais bien lorsque ces choses borderont
> les franges de ta vie sans empêcher que tu les surmontes, nu et
> libre.

Cette fois le thème de la liberté est explicite et rassemble les
différentes idées évoquées tout au long du livre. La liberté est donc un
ensemble tout entier qui fait partie de notre vie.

**La Raison et la Passion**

> Car la raison, si elle est seule à gouverner, est une force qui
> limite ; tandis que la passion, laissée à elle-même, est flamme qui
> brûle jusqu'à se détruire elle-même.

On observe ici encore une dualité qui se complète plus qui ne s'oppose.
Il faut associer les deux afin d'en tirer maximum parti.

**La Douleur**

> Ta douleur marque l'éclatement de la coquille qui enferme ta
> compréhension. Comme le noyau du fruit doit s'ouvrir pour que son coeur
> paraisse au soleil, tu dois connaître la douleur.

C'est selon moi une très belle définition poétique de la douleur, qui
propose que celle-ci nous montre nos propres limites. Une fois ces
limites dépassée la douleur n'existe donc plus.

**L'Enseignement**

> Si l'enseignant est sage, il ne t'ordonnera pas d'entrer dans la
> demeure de sa sagesse, mais te conduira plutôt au seuil de ton propre
> esprit.

L'enseignement est selon moi une qualité très importante car elle
permet de diffuser et de transmettre le savoir entre différentes
personnes, ce qui est vital pour la compréhension et la survie de
l'humanité. Ainsi selon Khalil Gibran, le bon enseignant n'est pas
celui qui nous guide vers son savoir mais celui qui nous guide vers
notre propre savoir.

**Connaissance de Soi**

> Ne dites pas : "J'ai trouvé la vérité", mais plutôt : "J'ai
> trouvé une vérité.

Il existe donc plusieurs vérités et on oublie souvent ceci. En effet, on
imagine souvent que la vérité est universelle et unique, or elle dépend
de plusieurs facteurs et une vérité ne saurait convenir à l'ensemble du
monde. Ceci est valable notamment pour sa propre personne mais beaucoup
moins dans des concepts scientifiques établis et prouvés.

**L'Amitié**

Notre ami n'est pas là pour combler un vide mais bien pour partager des
choses et faire en sorte que notre vie soit plus heureuse.

**La Parole**

> Vous parlez quand vous cessez d'être en paix avec vos pensées.
> Lorsque vous ne pouvez plus demeurer dans la solitude du coeur, vous
> vivez par les lèvres, et le son est une diversion et un passe-temps.
> Dans un grand nombre de vos propos, la pensée est à moitié estropiée._
> Cette citation en illustre bien une autre : _"la parole est d'argent
> mais le silence est d'or."

L'auteur propose donc ici que notre
dialogue intérieur est plus riche et plus puissant que notre dialogue
extérieur. Cela signifie aussi que nous pouvons trouver en nous les
réponses à nos questions.

**Le Bien et le Mal**

> Car Ce que que le mal sinon du bien torturé par sa propre faim et
> sa soif ?

Toujours ce principe de dualités contraires qui ne forment qu'une au
final.

> Mais que celui qui désire ardemment n'aille pas dire à celui qui
> désire peu : "Comment se fait-il que tu tardes et temporises ?" Car le
> vraiment bon ne demande pas au nu : "Où est ton habit ?", ni au sans
> logis : "Qu'est-il arrivé à ta maison ?

J'apprécie énormément ce principe de respect et de compréhension. Par
le silence de certaines questions on témoigne de notre respect envers la
personne, on évite un certain malaise et on passe pour une personne
bonne et vertueuse.

**Plaisir**

> Or le regret est obscurcissement de l'esprit, pas sa punition.

Ne jamais regretter est un principe qui me tient à coeur et je suis
content de voir que je suis soutenu par d'autres personnes. En effet,
regretter ne mène à rien sinon aux remords et à la tristesse. Le regret
ne changera pas les conséquences et il faut les accepter.

**La Beauté**

> Peuple d'Orphalese, la beauté est la vie quand celle-ci dévoile sa
> sainte face. Et c'est vous la vie et le voile, La Beauté c'est
> l'éternité qui s'observe dans un miroir Et vous êtes l'éternité et le
> miroir tout ensemble.

Nous sommes souvent nous-mêmes les réponses aux questions existentielles
que nous pouvons nous poser.

**Mort**

> Car la vie et la mort sont une, comme le fleuve et la mer.

On note une nouvelle fois la présence de la dualité entre deux concepts
opposés à première vue.
