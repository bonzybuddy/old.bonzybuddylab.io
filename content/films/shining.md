---
title: "The Shining"
date: 2021-02-02T20:17:31+01:00
draft: false
author: "Stanley Kubrick"
principal_actors: "Jack Nicholson, Shelley Duval, Danny Lloyd"
genres:
- Fantaisie
release_year: "1980"
cover: 
  img: /covers/shining-cover.jpg
  source: https://www.allocine.fr/film/fichefilm_gen_cfilm=863.html
---

## 🗣 De quoi ça parle

Un homme devient gardien dans un hôtel dans les montagnes aux Etats-Unis. Il est chargé de garder l'hôtel durant la période hivernale alors qu'il n'y a aucun client. L'hôtel jouit d'une très mauvaise réputation car il serait hanté par l'âme de l'ancien gardien qui a tué sa famille, c'est la raison pour laquelle personne n'accepte ce poste. Comme le protagoniste manque d'argent (il est aussi auteur), il est prêt à prendre n'importe quel job. Il s'y installe donc avec sa femme et son fils. Le fils est doté d'un pouvoir dit le "shining" qui permet de communiquer avec les autres détenteurs de ce pouvoir sans parler.

La situation s'avère normale au début mais tout s'aggrave au fil des mois...

## 🔎 Comment je l'ai découvert

Il s'agit d'un film culte et il faisait partie de ma liste de films à voir absolument. Je le connaissais de nom.

## 👏 Ce que que j'ai aimé

Les techniques de filmage sont élaborées car Kubrick est un grand réalisateur. La technicité est au rendez-vous.

## 👎 Ce que que je n'ai pas aimé

Ce n'est pas du tout le genre de film que j'apprécie car je n'aime pas les films qui font peur.

## 🎯 Qui l'apprécierait

Ceux qui apprécient les films d'horreur ou du moins ceux qui font peur.

## 📽 Films reliés

Les autres films de Stanley Kubrick
