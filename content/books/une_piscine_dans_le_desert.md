---
title: "Une piscine dans le désert"
date: 2021-04-04T12:09:14+02:00
# maj: "08-02-2021"
draft: false
genre: 
- Roman
author: "Diane Mazloum"
fiction: true
release_year: "2020"
# cover: 
#   img: /covers/unepiscinedansledesert-cover.jpg
#   source: https://sive.rs/a/
---
## 🗣 De quoi ça parle

Un Québecois se rend au Liban car une piscine a été construite illégalement sur le terrain de sa famille. Il compte vendre le terrain ou bien racheter la piscine pour régler cette affaire et gagner la reconnaissance de son père. Il rencontre donc les propriétaires afin de négocier. 

## 🔎 Comment je l'ai découvert

Dans Télérama

## 💭 Ce que j'en ai pensé

Je n'ai pas vraiment apprécié ce livre. Il était assez court (100 pages) mais il ne se passe pas grand chose. L'histoire est complétement farfelue et il n'y a pas de dénouement à la fin. Le fait qu'il ne se passe rien est peut être voulu du fait que l'histoire se déroule dans une région du Liban où effectivement il ne se passe rien la plupart du temps.

## 🎯 Qui l'apprécierait

Ceux qui aiment les livres courts et sur le Liban.

## 📚 Livres reliés

Les autres livres de Diane Mazloum, notamment *L'Age d'or* que j'ai beaucoup apprécié
