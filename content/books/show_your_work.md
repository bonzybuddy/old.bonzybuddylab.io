---
title: "Show Your Work!"
date: 2021-02-03T11:32:05+01:00
draft: false
author: "Austin Kleon"
genres:
  - Business
  - Créativité
  - Life Advice
release_year: "2014"
cover: 
  img: /covers/show-your-work-cover.jpg
  source: https://austinkleon.com/show-your-work/
---

## 🥇🥈🥉 Le livre en 3 phrases

1. Partager ses idées et son travail, sur internet, gratuitement
2. Il ne faut pas nécessairement être un expert pour partager son travail, les débutants peuvent facilement aider les autres.
3. En partageant son travail en ligne, vous attirez des gens qui sont intéressés par ce que vous faites, ce qui représente une grande satisfaction personnelle.

## 🎣 Impressions

J'ai bien aimé ce livre qui m'a poussé à débuter sur Internet alors que je me posais des questions sur l'utilité et la manière de procéder. C'est un livre très court qui peut se lire en une soirée mais qui est un condensé de conseils pour arriver à ses fins.

## 🎯 Qui devrait le lire

Ceux qui souhaitent partager leurs créations, leur travail ou leurs idées notamment sur internet mais qui hésitent à se lancer. Si vous pensez que le monde ne veut pas de votre travail ou qu'il ne vaut pas la peine de le publier, lisez ce livre.

Ceux qui ne sentent pas à l'aise avec le fait de partager leur travail ou leurs idées.

Ceux qui aiment les livres courts et faciles à lire.

## 🎉️ Ce que que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- Il a contribué au lancement de mon blog, de mon podcast et de ma chaîne YouTube
- Il m'a aidé à démistifier le mythe de l'expert : un débutant peut facilement partager son travail et contribuer à aider les autres
- Il m'a permis de me sentir plus à l'aise avec le fait de me montrer sur internet
- Le fait de commencer ce blog est une grande satisfaction car c'est une chose que je voulais faire depuis longtemps.

## 💬 Mes 4 citations préférées

> The real gap is between doing nothing and doing something.

> We can stop asking what others can do for us, and start asking what we can do for others.

> Carving out a space for yourself online, somewhere where you can express yourself and share your work, is still one of the best possible investments you can make with your time.

> The minute you learn something, turn around and teach it to others. Share your reading list. Point to helpful reference materials. Create some tutorials and post them online. Use pictures, words, and video. Take people step-by-step through part of your process. As blogger Kathy Sierra says, “make people better at something they want to be better at”

## 📝 Résumé et notes personnelles

Voici un résumé des différents chapitres qui m'ont le plus intéressé et marqué.

### Introduction : Une nouvelle manière d'agir

Le but de toutes nos recherches et nos pensées est de les partager avec les autres car ce qui nous a aidé ou intéressé peut sûrement servir à d'autres personnes.

En partageant votre travail vous devenez capables d'aider ou de contribuer au bonheur des autres, ce qui vous rend heureux à votre tour. Vous rentrez alors dans un cercle vertueux.

L'avènement d'Internet a grandement facilité le partage des informations et de son travail, gratuitement et avec un très large public puisqu'il s'agit du monde entier.

En publiant votre travail vous montrez ce dont vous êtes capables, ce que vous avez réalisé et quels sont vos centres d'intérêt. Proposer cela à votre futur employeur est bien plus valorisant et efficace qu'un CV. Vous pourriez faire de nouvelles rencontres avec des gens qui partagent les mêmes passions que vous, ce qui pourrait changer votre vie. Vous vous créez par la suite une _fanbase_ qui vous soutient et s'intéresse à votre travail.

Tout ce que vous avez à faire est de partager votre travail.

Ce sont ces idées qui m'ont convaincu de commencer mon blog car je souhaite contribuer et apporter mes solutions.

### 1. Vous n'avez pas besoin d'être un génie

De nombreuses personnes ne se lancent jamais car elle ne se sentent pas assez compétentes. C'est ce que l'on appelle le mythe de l'expert. Arrêtez de vous demander ce que font les autres pour vous et commencez à vous demandez ce que vous pouvez faire pour les autres.

Un amateur sera plus à même d'aider un autre amateur car il aura problablement rencontré le même problème récemment et comprendra mieux sa manière de penser. L'expert a généralement oublié les réponses aux questions simples du débutant et parfois même les dédaigne car elles ne lui apportent rien. L'expert répond aussi souvent de manière complexe ce qui n'aide pas le débutant.

Le vrai écart se situe entre ne rien faire et faire quelque chose, aussi insignifiante soit-elle.

N'attendez pas de devenir un expert dans un domaine pour en parler, vous risquez de ne jamais le devenir et donc de ne jamais en parler.

Trouvez un sujet que vous souhaitez apprendre. Et partagez la manière dont vous l'apprenez, les résultats que vous obtenez mais aussi les échecs. Ce faisant, vous aidez d'autres personnes intéressées par le même sujet à se lancer.

### 2. Partagez aussi le processus

Grâce à la facilité de publication sur Internet, vous pouvez désormais partager tout le processus d'apprentissage et d'évolution et pas uniquement le produit final. Montrer uniquement ce dernier n'aidera pas les autres à vous suivre car le chemin pour y arriver leur semblera trop long et trop compliqué. Tandis qu'en montrant la manière dont vous y êtes arrivé, vous décomposez le processus en étapes et facilitez la compréhension pour votre public.

Si vous écrivez un livre, ne vous contentez pas de montrer le livre terminé mais au contraire, comment vous avez fait pour l'écrire, les brouillons, les notes, les réécritures etc.

### 3. Partagez un petit peu chaque jour

Partagez les choses que vous jugez utiles pour les autres mais ne sur-partagez pas sinon vous noyez les informations utiles. Si vous pensez pouvoir aider ne serait-ce qu'une seule personne dans le monde avec votre idée, partagez-la.

En achetant votre nom de domaine, vous vous attribuez un espace personnel propre à vous. Vous pouvez y écrire ce que vous voulez, vous n'êtes pas dépendant d'une plateforme tierce.

Ce chapitre m'a fait acheter mon nom de domaine sur lequel se trouve ce blog, et j'en ai profité pour tout créer moi-même au lieu de faire appel à des instances comme Wordpress ou Ghost qui limitent ma créativité ou bien sont trop chers. Cela m'a certes pris du temps mais aussi permis d'apprendre plein de nouvelles choses que je peux désormais partager afin d'aider ceux qui se trouveraient dans la même situation que moi un jour. J'ai heureusement pu m'appuyer sur les connaissances d'amis doués en informatique (que je remercie grandement) mais si vous n'avez pas cette chance, vous trouverez de nombreuses ressources sur internet ou vous pouvez me contactez en gardant à l'esprit que je ne suis pas un expert ^^

### 4. Partagez aussi votre cabinet de curiosités

Vous pouvez bien sûr partager le travail des autres que vous trouvez inspirant et intéressant. Cela leur permettra d'augmenter leur visibilité en mixant les publics touchés. Ce qui vous vaudra d'être cité par d'autres créateurs.

Demandez-vous ce que vous lisez, écoutez, regardez, pensez, d'où vous vous inspirez.

Créditez le travail des autres au moyen d'un lien afin de rendre l'information facile d'accès.

### 5. Racontez de belles histoires

Améliorez la manière dont vous racontez quelque chose, que ce soit à l'oral ou à l'écrit. Soyez brefs, concis.

N'hésitez pas à parler de vous, de ce que vous faites, cela vous donnera une chance de diffuser votre travail et de faire de nouvelles rencontres potentiellement très enrichissantes (dans les 2 sens du terme).

### 6. Enseignez ce que vous savez

En partageant vos savoirs de manière pédagogique vous contribuez à fidéliser votre public et à le faire se sentir proche de vous. Vous les incitez à suivre les mêmes chemins que vous afin de parvenir aux mêmes résultats

### 7. Ne devenez pas un _spam_

Ne vous souciez pas du nombre de personnes qui vous suivent ou qui regardent votre travail. Ne vous perdez pas dans les statistiques.

Si vous souhaitez être intéressant vous devez d'abord être intéressé.

Ne demandez jamais, jamais, aux gens de vous suivre. Ils le feront naturellement s'ils estiment que vous en valez la peine. Ne vous transformez pas en mendiant qui quémande des abonnés et des likes.

Prenez la peine de rencontrez vos amis ou vos abonnés en _face-to-face._ Cela sera l'opportunité de tisser des liens plus forts que par le simple intermédiaire d'un ordinateur et peut être même de développer une amitié.

### 8. Apprenez à encaisser les coups

Lorsque vous partagez votre travail en ligne vous vous exposez à des critiques. Apprenez à les accepter et à en faire abstraction. Si vous vous focalisez sur ces commentaires vous allez vous minez et serez enclin à abandonner. Les _haters_ ne consituent qu'une minorité et ne sont généralement jamais des créateurs eux-mêmes ce qui explique leurs actes et leur virulence.

### 9. Faites de l'argent

Il ne devrait y avoir aucun problème à demander de l'argent pour un travail. Il faut sortir du mode de pensée romantique de l'artiste qui meurt de faim.

Mais soyez prudent quant à faire de l'argent à partir de votre travail.

Personnellement je refuse d'afficher des pubs ou de faire des placements de produits qui dénaturent mon travail. Je préfère laisser le choix de faire des dons et garder mon travail public et gratuit.

Si vous devenez connu, aidez ceux qui vous ont aidé à en arriver là. Remerciez-les.

Le modèle de la liste de diffusion est appliqué par de nombreux entrepreneurs avec beaucoup de succès. Conservez les adresses mails des personnes qui apprécient votre travail et qui pourraient être intéressés par un article que vous vendriez un jour.

### 10. Continuez

Continuez à partager votre travail tout au long de votre vie, ne laissez pas tomber.

Prenez des pauses de plusieurs jours ou semaines, ne devenez pas esclave de votre propre travail, ne laissez pas vous imposez un rythme effréné.

Cela rejoint le livre de Timothy Ferriss "The 4-Hour Workweek" qui conseille de prendre des mini retraites afin de souffler et de penser à autre chose.
