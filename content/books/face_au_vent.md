---
title: "Face au vent"
date: 2021-04-04T12:05:50+02:00
# maj: "08-02-2021"
draft: false
genre: 
- Roman
- Voyage
author: "Jim Lynch"
fiction: true
release_year: "2016"
# cover: 
#   img: /covers/face-au-vent-cover.jpg
#   source: https://sive.rs/a/
---

## 🗣 De quoi ça parle

Une famille américaine d'origine islandaise qui habite à Seattle. Leurs vies tournent autour des bateaux et de la voile. Le père, un des fils et le grand-père travaillent sur un chantier naval. La mère est prof de physique dans un lycée et transmet sa passion de l'astrophysique et de la dynamique des fluides à ses 3 enfants. La cadette, Ruby, est une prodige de la voile mais ne veut pas y faire carrière. L'aîné, Bernard, devient anarchiste et part découvrir le monde en bateau. L'histoire est vue à travers le personnage de Josh, le 2e fils. Il parle beaucoup de sa famille et de ses echecs amoureux avec ses rancards. 

## 🔎 Comment je l'ai découvert

Ma mère me l'a offert pour mon anniversaire

## 💭 Ce que j'en ai pensé

Les premiers chapitres sont entièrement consacrés à la voile avec des termes techniques dans lesquels je me retrouve tout à fait, ce qui m'implique dans l'histoire. Mais ensuite, l'histoire principale devient prépondérante et cela m'a moins intéressé.

L'histoire en elle-même est un peu triste et farfelue, avec une chronologie alambiquée ; on ne sait jamais trop à quelle moment se passe l'histoire que raconte le personnage.

J'ai quand-même globalement apprécié car il m'a rappelé que j'adorais la voile et tout ce qui se passe autour des bateaux.

## 🎯 Qui l'apprécierait

Les passionnés de voile et de physique

## 📚 Livres reliés

Très techniques mais les livres de Bertrand Chéret sur la voile.
