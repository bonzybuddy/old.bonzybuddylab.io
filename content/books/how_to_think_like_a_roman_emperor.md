---
title: "How to think like a Roman Emperor"
date: 2021-01-25T23:23:04+01:00
draft: false
author: "Donald Robertson"
genres:
  - Philosophie
  - Life Advice
release_year: "2019"
cover: 
  img: /covers/roman-emperor-cover.jpg
  source: https://donaldrobertson.name/how-to-think-like-a-roman-emperor-the-stoic-philosophy-of-marcus-aurelius/
---

## 🥇🥈🥉 Le livre en 3 phrases

1. La philosophie Stoïque nous aide dans la vie quotidienne
2. Le Stoïque se comporte en homme sage, mesuré et vertueux
3. Le Stoïcisme permet d'avoir une grande ouverte d'esprit sur soi-même, les autres et le monde

## 🎣 Impressions

Le livre est un peu long avec un début pas terrible mais de nombreux conseils sont donnés afin d'appliquer au milieu la philosophie Stoïque au travers d'exemples et de mentors comme Marc Aurèle ou Sénèque.

Indéniablement un livre qui a complètement bouleversé ma façon de penser et de voir les choses.

## 🔎 Comment je l'ai découvert

C'est une recommandation d'Ali Abdaal et de son ami The Stoic teacher.

## 🎯 Qui devrait le lire

Ceux qui souhaitent découvrir un nouveau mode de pensée et d'observation des événements. Egalement ceux qui sont un peu coincés d'esprit.

## 🎉 Ce que que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- Ce livre m'a permis de découvir la pensée philosophique Stoïque, au travers de Marc Aurèle et de ses maîtres comme Epictète, Sénèque, Xeno et bien d'autres. Il m'a fait réfléchir sur moi-même et sur la manière dont je perçois le monde et les autres individus. Ainsi, j'ai appris de nombreux principes stoïques qui sont à mon avis très utiles dans notre vie quotidienne et qui permettent d'atteindre un statut d'homme sage et vertueux. Ce statut représente pour les stoïques la forme du bonheur a rechercher. J'ai aussi découvert que je réfléchissais déjà comme un Stoïque pour plusieurs choses. Ceci me conforte dans l'idée que c'est le chemin de vie qui me convient, du moins en partie, et qui me permettra d'accomplir mes objectifs. C'est un tout nouveau mindset que j'ai appris. Avoir pu mettre des mots et des idées sur des concepts que je ne connaissais que vaguement m'a permis de conforter ma précédente manière de penser mais aussi de la mettre à jour profondément.

- Dorénavant j'essaie de réfléchir comme un Stoïque tout au long de ma journée afin de faire les meilleurs choix. Mon comportement, mes idées, ma vie est entièrement plus "sage" et me permet d'accepter n'importe quel défi. Bien sûr ce n'est que par un processus d'apprentissage long et complexe que j'atteindrai des résultats très probants mais le livre m'a néanmoins incité à m'intéresser à ce courant de philosophie et d'en percevoir la puissance. J'ai envie d'appronfondir le sujet et de lire d'autres livres pour bien m'imprégner et décider si je veux continuer sur ce chemin.

- Je ne suis pas forcément d'accord avec tous les principes mais quand-même avec la plupart.

## 💬 Mes 3 citations préférées

> It is not things that upset us but our judgements about them

> For Stoics, virtue is still the only true good

> The Stoics adopted the Socratic division of cardinal virtues into wisdom, justice, courage and moderation

## 📝 Résumé + notes personnelles

Le livre aborde plusieurs thèmes du Stoïcisme à travers la vie de Marc Aurèle, empereur romain au IIème siècle ap. J-C.

Après une introduction sur sa vie, l'auteur commence par décrire l'histoire du Stoïcisme, ses fondateurs et ses principaux principes : la sagesse, la vertu, la mesure, le courage et la justice. Le Stoïque vit simplement mais bien, il n'a pas besoin de beaucoup de choses pour vivre, il y a là un certain penchant pour le minimalisme, courant de pensée qui m'intéresse en ce moment (après la lecture de La Clinique du Coureur par Blaise Dubois). De plus, le Stoïque utilise correctement ce dont il a disposition contrairement à quelqu'un qui aurait tout mais sans savoir utiliser quoique ce soit.

Les Stoïques voient les individus comme des citoyens du monde, tous sont nos frères et soeurs, une idée que l'on retrouve dans les religions monothéistes.

Les Stoïques ne sont pas pour autant démunis d'émotions contrairement au sens du mot dans le langage courant où stoïcisme s'écrit avec une minuscule.

Le chapitre suivant traite de la contemplation de l'homme sage et comment copier ses actes. Selon les professeurs de Marc Aurèle, l'homme sage, ce dont à quoi aspire le Stoïque, est un homme qui accepte volontiers les critiques de ses amis afin de reconstruire son individu, le regard d'un ami est extérieur et permet de percevoir des défauts de notre personnalité difficiles à déceler par nous-même. Au lieu de s'écarter des gens qui nous critiquent nous devrions nous en rapprocher afin d'écouter leurs critiques. Pour autant il ne faut pas forcément nous changer entièrement sur la base des critiques des autres. Il faudrait d'abord s'assurer qu'il s'agisse d'une critique constructive et en adéquation avec la sagesse. Sinon il s'agit d'une critique vaine et dénuée de sens, peut-être même teintée de jalousie. Si elle émane d'un ami elle sera considérée avec plus de valeur que si elle est donnée par un inconnu. En effet, notre ami sera plus enclin à vouloir notre bien qu'un inconnu.

L'auteur donne l'exemple du professeur de langue de Marc Aurèle, Alexandre of Cotiaeum qui ne critiquait jamais une personne ouvertement si elle se trompait de mot, ni ne la coupait dans son discours. Au contraire, il opérait d'une manière beaucoup plus subtile qui consistait à réutiliser le mot erroné dans sa réponse mais de manière correcte. Ainsi il corrigeait l'orateur indirectement.

Enfin, le Stoïque est ouvert d'esprit ce qui lui permet de changer sa pensée si on lui donne des arguments corrects et valides qui prouvent son tort. Il accepte volontiers l'idée de changement.

Le chapitre suivant décrit notre manière à suivre nos valeurs. L'auteur propose de méditer chaque matin sur comment aborder la journée et d'en faire un topo le soir. Ceci permet de préméditer l'adversité que nous sommes susceptibles de rencontrer chaque jour et de ne pas être surpris par celle-ci. Nous pouvons ainsi nous préparer mentalement (et physiquement avec le concept d'inconfort volontaire qui nous met volontairement dans une situation d'inconfort afin que notre corps s'habitue) à toutes les épreuves de la vie. Tout au long de notre journée, nous nous efforçons de réagir comme un Stoïque et le soir avant de s'endormir nous repassons les bons et les mauvais événements afin de trouver des solutions à long terme pour que cela n'arrive plus. Ainsi nous nous endormons paisiblement, sans trouble.

Le chapitre suivant est intimement lié au précédent et nous apprend comment vaincre la peur. Pour le Stoïque il n'y a rien de fondamentalement bien ou mal, ainsi ce ne sont pas les choses en elles-mêmes qui nous contrarient mais la manière dont on les voit.
L'auteur reprend ensuite le principe de préméditation de l'adversité et le détaille longuement. Il précise également qu'il s'agit de méthodes utilisées aujourd'hui par les psychologues pour soigner leurs patients (comme de nombreuses autres méthodes de la philosophie Stoïque). Les méthodes décrites sont les suivantes :

Une habituation à l'émotion permet de diminuer l'intensité de celle-ci (inocuité, image du vaccin)

Une acceptation des événements désagréables (anxiété ou douleur par exemple) et une vision détachée vis-à-vis de ceux-ci afin de les considérer avec indifférence et de les oublier.

*Cognitive distancing*, ce ne sont pas les choses en elles-mêmes qui nous contrarient mais la manière dont on les voit. *The Universe is change : life is opinion*, la qualité de notre vie est déterminée par nos jugements car ce sont eux qui façonnent nos émotions. Le but du Stoïque est de changer nos opinions sur les événements externes plutôt que juste nos émotions.

*Dédramatiser*, reformuler nos jugements initiaux pour passer du *What If ?* au *So what if this happens ?* Ce n'est pas la fin du monde. Selon les psychologues, écrire une page sur un sujet qui nous fait peur (par ex. la perte de notre emploi et ce qu'il en suivra) et de la relire par la suite permet de mieux prévenir le choc et de dédramatiser la situation si celle-ci arrive. Se projeter dans le temps permet aussi de dédramatiser, se demander si cette situation sera importante à nos yeux dans 10 ou 20 ans. Si elle ne l'est pas alors il n'y a pas de raison de s'en inquiéter aujourd'hui.

*Trouver des solutions*

*Behavioral rehearsal* ou répétition du changement de comportement, qui stipule que notre aptitude à surmonter s'améliore avec l'expérience et les essais.

*Worry postponement* ou comment remettre à plus tard l'anxiété, une étude faite sur des étudiants qui consistait à repérer les moments de la semaine où ils devenaient stressés et de reporter toutes leurs pensées de stress à un moment précis *(worry time)* permet de diminuer ce stress.

Le dernier chapitre nous aide à surmonter notre colère, qui n'est pas vue comme une qualité. Les proto-passions sont les émotions que nous ressentons dans les secondes survenant un évènement fâcheux, ce sont des réactions automatiques et innées de notre esprit qui peuvent ne pas être celles d'un homme sage. Pourtant il ne s'agit pas de supprimer ces proto-passions mais de les laisser apparaître naturellement et les faire disparaître une fois que notre raison a repris le contrôle. Les différentes manières de contrôler la colère sont les suivantes :

*Self-control*, détection des signes pré-colère et les faire disparaître immédiatement.

*Cognitive distancing*, ce ne sont pas les choses en elles-mêmes qui nous contrarient mais la manière dont on les voit.

*Postponement*, remise à plus tard. Agir une fois que l'on a la tête froide et pas dans le vif de l'action où nos pulsions pourraient nous mener vers les mauvais choix.

Utiliser un modèle de vertu et de sagesse. Se demander qu'aurait fait Marc Aurèle, Socrate ou Zeno (fondateur du Stoïcisme) à notre place. Avoir ce modèle en tête permet de nous soutenir mentalement. Se dire que si une personne l'a réussi alors nous aussi nous sommes capables de le reproduire. Après tout ce n'était qu'un être humain, si antique soit-il.

Analyse fonctionnnelle, se projeter les effets de notre colère sur le moyen ou long terme et faire le pour et le contre, quelles seront les conséquences si j'agis de la sorte ? Je dois rester vertueux et sage, je ne dois pas agir de cette manière.

Marc Aurèle était très tolérant dans ses jugements, il ne punissait pas sévèrement, il pardonnait, il ne s'énervait pas contre les autres malgré leurs actes.

La rencontre avec des ennemis ou des gens malhonnêtes ne doit pas se faire dans la colère mais de manière sage afin de tester notre capacité à exercer notre *self-control* et notre modération, mesure, sagesse.

>Quand tu pointes un doigt vers quelqu'un rappelle toi que trois autres sont dirigés vers toi.

Enfin, la dernière stratégie du Stoïque est le déterminisme, l'homme sage qui voit le monde de manière rationnelle n'est jamais surpris par rien.
