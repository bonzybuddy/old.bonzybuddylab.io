---
title: "Getting Things Done"
date: 2021-03-07T15:40:25+01:00
# maj: "08-02-2021"
draft: false
genres: 
- Productivité
- Life Advice
author: "David Allen"
fiction: false
release_year: "2001 réédité en 2015"
cover: 
  img: /covers/gtd.png
  source: https://gettingthingsdone.com/
---
*Information importante : Getting Things Done, GTD, Our brain is designed for having ideas not for storing them sont des marques déposées de David Allen Company (DAC), je ne possède aucun droit ou contenu sur ce livre, il s'agit simplement de ma review personnelle sans aucun caractère commercial ou autre.*

## 🥇🥈🥉 **Le livre en 3 phrases**

- S'organiser pour bien gérer son travail et ses projets
- Appliquer la méthode GTD (CCORE)
- Etre le plus productif possible tout en diminuant son stress

## 🎣 **Impressions**

Ce livre est souvent cité comme étant une référence en matière de productivité et je dois dire que j'ai été agréablement surpris. Il fournit une méthode très efficace et de nombreuses indications permettant de bien la comprendre et de l'appliquer correctement. Facile à lire et bien structuré, il dispose d'un glossaire ainsi qu'un index, ce qui est très appréciable pour retrouver un terme. J'aime l'idée de la simplicité derrière cette méthode.

## 🔎 **Comment je l'ai découvert**

C'est un ouvrage inévitable lorsque l'on s'intéresse à ce domaine, n'importe quel *productivity nerd* vous le conseillera. De nombreux autres livres se sont inspirés de ce modèle paru en 2001 puis réédité en 2015.

## 🎯 **Qui devrait le lire**

Toute personne aspirant à être plus productive, à apprendre une nouvelle méthode d'organisation, à ne plus se laisser submerger par ses projets. Et dans une certaine mesure à sortir de la procrastination ^^

## 🎉 Ce que le livre a changé chez moi

### Comment ma vie / comportement / idées / pensées a évolué après la lecture de ce livre.

- J'applique dorénavant la fameuse méthode CCORE dans ma vie quotidienne comme pour mes projets (ce blog par exemple).
- J'utilise de nouveaux "outils" comme l'application *Notion* ou beaucoup plus simple comme un carnet et un crayon à côté de mon lit, dans mes sacs etc. Je ne me soucie plus d'oublier ou de passer à côté de quelque chose d'intéressant car je note tout. J'utilise une checklist avant de partir en voyage.
- J'ai revu et corrigé ma manière de prendre des notes sur le vif, de noter des choses que j'oublie facilement mais qui pourraient être réutilisées. Développer un projet devient alors plus simple et je perds moins de temps à chercher où j'ai rangé mes idées.

## 💬 Mes 3 citations préférées

> There is usually an inverse relationship between how much something is on your mind and how much it's getting done

> Our brain is designed for having ideas not for storing them

> Filling has to be instantaneous and easy

## 📝 Résumé + notes personnelles

L'ouvrage est divisé en 15 chapitres mais ce résumé ne s'attardera que sur les 5 qui constituent la méthode CCORE (*Capture, Clarify, Organize, Reflect, Engage*). Le reste est intéressant mais j'essaie de rester synthétique et d'aller à l'essentiel. Si vous appréciez le sujet, je vous invite à lire les autres chapitres.

Vous connaissez sûrement ces 5 étapes, mais il probable que vous ayez des lacunes dans au moins une d'entre elles.

Les outils que vous utiliserez pour appliquer le CCORE n'ont aucune importance, ils peuvent être virtuels et complexes comme des applications ou très simples comme une feuille et un crayon. Ce qui compte c'est la manière dont vous allez vous servir de cet outil. Le seul hic est de trouver ses outils et la manière de les utiliser, cela peut prendre un certain temps mais il en vaut la peine. Ne vous découragez pas et sachez que vous pouvez toujours le faire évoluer par la suite, rien n'est figé.

### C pour *Capture* (Saisir)

**"Ah j'ai oublié ça !" "Je devais faire ce truc mais je ne me rappelle plus lequel !"**  Si vous vous reconnaissez dans ces phrases lisez la suite 😉

La première étape de la méthode est celle qui m'a le plus touché. Le but est de noter TOUT ce que vous avez à faire. 100% des choses qui nécessitent une action de votre part doivent être écrites et faciles d'accès. Notre cerveau est fait pour penser, pas pour retenir. Si vous comptez sur votre mémoire, vous oublierez forcément une partie (on ne peut retenir qu'environ 7 choses simultanément). Libérer son esprit de la contrainte d'avoir à se remémorer est une chose simple mais si efficace !

Votre système doit être rapide, fonctionnel et amusant sinon vous ne l'utiliserez plus. Noter une idée doit être instantané et facile. Pour cela, j'ai toujours sur moi une feuille de brouillon et un crayon ou bien une page vierge dans mon appli de notes.

### C pour *Clarify* (Clarifier)

**"J'ai noté ce truc mais je ne sais pas à quoi ça correspond !"** Si vous vous reconnaissez dans cette phrase lisez la suite 🙄

Le point clé de la clarification est de savoir tout de suite qu'elle est l'action immédiate permettant d'accomplir la tâche. Par exemple vous avez noté "Dentiste" sur votre liste d'actions, l'action immédiate est : "Appeler le cabinet pour fixer le rendez-vous". C'est cette action uniquement qui vous permettra de l'accomplir.

Adoptez la règle des 2 minutes. Elle est très simple. Une tâche demande moins de 2 minutes pour être réalisée ? Faites-la immédiatement et vous serez soulagé. Vous pouvez changer la durée en 1 minute ou 3 minutes mais je vous conseille de rester en deçà de 5 minutes. 

Si la tâche demande plus de temps, notez-la et programmez-la pour ne pas l'oublier.

Ne dupliquez pas des informations que vous avez déjà ou que vous pouvez rapidement retrouver sur Internet. 

### O pour *Organize* (Organiser)

**"J'ai noté mais je ne sais pas où je l'ai rangé !"** Si vous vous reconnaissez dans cette phrase lisez la suite 🙃

Les outils nécessaires sont : une liste des choses à faire, un calendrier, une liste d'actions en attente, une liste un-jour/peut-être et un dossier de rangement général trié par ordre alphabétique par exemple.

Gardez-vous votre boîte aux lettres remplie ? Non. Vous relevez votre courrier, le lisez et en fonction de l'information donnée par la lettre vous lui affectez une tâche ou un rangement. Faites pareil avec votre boîte mail. Chaque mail utile (ou à défaut, l'information qu'il contient) convient d'être rangé dans un endroit/dossier approprié. Et ceux qui n'apportent rien doivent être supprimés.

Votre environnement de travail doit être le plus clair possible, avec tout ce dont vous avez besoin sous la main. Notamment du papier, un stylo et une liste des choses à faire. Si votre bureau est encombré de mille bricoles vous n'aurez pas envie de vous y attablez et de commencer à travailler. Cela vaut aussi pour votre atelier si vous bricolez ou votre home-studio si vous êtes musicien.

### R pour *Reflect* (Réfléchir)

**"Je suis perdu dans tout ce que je dois faire !"** Si vous vous reconnaissez dans cette phrase lisez la suite 😌

Si vous attendez un outil miracle, sachez qu'il n'en existe pas. Les miracles n'existent quasiment jamais. Vous devez juste trouver l'outil qui vous est adapté et avec lequel vous arrivez facilement à travailler. Il doit répondre à ces deux critères : permettre de noter vos projets et retrouver facilement et rapidement tous les composants liés à un projet.

Les informations qui ne nécessitent pas d'action de votre part sont regroupées en 3 catégories:

- information générale utile
- aucune action sur le moment mais peut-être plus tard
- information non utile (à jeter)

Revoir son système régulièrement est très important. Sans ça vous serez vite dans le flou. L'auteur propose de revoir son système chaque semaine afin de recouper toutes les informations de la semaine passée et de réfléchir sur celle à venir. Lors de cette revue vous rassemblez toutes vos notes, idées, choses que vous avez eu durant la semaine et vous mettez  à jour vos listes, les nettoyez, les complétez et les actualisez.

Vous devez avoir confiance en votre système. S'il est à jour et complet vous serez apaisés et vous pourrez compter dessus. Sinon vous l'abandonnerez vite.

### E pour *Engage* (Entreprendre)

**"Je ne sais pas quoi faire et quand !"** Si vous vous reconnaissez dans cette phrase lisez la suite. 😎

Dans cette dernière étape, le but est simple : faire les choses. Les 4 critères a prendre en compte pour déterminer quoi faire sont :

- le contexte, en fonction de l'endroit où on se trouve et des outils que l'on a à disposition
- le temps disponible, avez-vous 10mn, 2h ou bien une journée entière de libre ? Cela devrait vous aiguillez dans votre choix,
- l'énergie disponible, il est 20h et vous êtes fatigué ? faites quelque chose de rapide qui ne requiert pas beaucoup de facultés mentales. Si au contraire il est 8h et que vous êtes bien reposé, adonnez-vous à un travail plus complexe.
- la priorité, quelle tâche nécessite d'être accomplie tout de suite ? quelles sont les plus importantes ?

Avec ces 4 questions vous devriez être capable de choisir quelle tâche faire à quel moment.

Après avoir lu cette review, vous voilà fin prêt à mener à bien tous vos projets et votre travail sans être submergé et constamment réfléchir à ce que vous avez à faire plutôt qu'à le faire.