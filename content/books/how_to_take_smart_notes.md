---
title: "How to take smart notes"
date: 2021-02-06T17:33:17+01:00
draft: false
author: "Sonke Ahrens"
genres:
  - Productivité
  - Etudes
  - Ecriture
release_year: "2017"
cover: 
  img: /covers/take-smart-notes-cover.jpg
  source: https://takesmartnotes.com/
---

## 🥇🥈🥉 Le livre en 3 phrases

1. Adoptez un nouveau mode de prise de notes, plus efficace. Gardez toujours un bout de papier à côté lorsque vous lisez afin de noter les choses qui vous paraissent intéressantes et utiles. Ces notes doivent être écrites à partir de vos propres phrases, ne recopiez pas le texte.
2. A partir de ces notes prises sur le vif, transformez-les en *Notes permanentes.* Celles-ci doivent être suffisamment concises et claires pour tenir sur une page A4. Ajoutez-y la référence. Développez l'idée originale pour la faire correspondre à vos recherches personnelles, incluez-là dans un contexte.
3. Ces *Notes permanentes* constituent ce que l'auteur appelle votre *Slip-box.* Cette dernière est l'endroit où vous conservez toutes vos notes développées qui s'articulent autour d'un sujet principal. Chaque sujet contient différentes notes provenant de différentes sources et contribuent à la compréhension globale du sujet et à l'élaboration d'écrits académiques ou autres.

## 🎣 Impressions

J'ai trouvé ce livre moyen mais à mon avis, cette méthode de prise de notes est bien plus efficace que n'importe quelle autre. Elle permet de garder une trace de ce que l'on lit ou regarde et de relier cela à nos recherches. 

Selon l'auteur c'est également la méthode la plus simple mais je pense qu'elle nécessite un minimum d'engagement et d'une période de test avant de pouvoir l'appliquer (comme toutes les habitudes). 

La plupart des infos utiles sont données dès les premières pages du livre et le reste est principalement constitué d'anecdotes ou de preuves de l'efficacité de la méthode. De plus, comme l'auteur le précise lui-même en début d'ouvrage, une bonne partie des idées présentées proviennent du livre *Getting things done* de David Allen (dont la *review* est disponible <a href="https://ilone.hayek.fr/books/getting_things_done/" target=_blank >ici</a>)

Le livre est par ailleurs très bien documenté avec de nombreuses références d'études.

## 🎯 Qui devrait le lire ?

Principalement les étudiants, les doctorants, les chercheurs, les auteurs et ceux qui écrivent ou lisent beaucoup. Car la méthode de la Slip-box vaut pour quelqu'un qui souhaite développer un sujet de manière approndie par l'intermédiaire d'une thèse, d'un compte-rendu ou d'un livre. 

Néanmoins la méthode peut parfaitement convenir à quiconque souhaite avoir une (très) bonne compréhension d'un ou plusieurs sujets pour lui-même ou dans l'optique de publier un —court— article sans qu'il soit évalué.

## 🎉 Ce que que le livre a changé chez moi ?

### How my life / behaviour / thoughts / ideas have changed as a result of reading the book.

- Le livre m'a fait connaître une toute nouvelle méthode de prise de notes, basée sur le système de Luhmann et améliorée par Zettelkasten pour ceux qui prennent des notes sur un ordinateur.

## 📝 Résumé + Notes personnelles

Tout le monde écrit. Notamment les étudiants, les chercheurs et les auteurs. Mais pas seulement. Nous écrivons pour retenir et apprendre. Le but de ce livre est de nous présenter une méthode efficace de prise de notes qui s'intègre dans n'importe quelle démarche impliquant l'apprentissage. 

Grâce à cette méthode, non seulement vous pourrez apprendre et retenir plus facilement mais vous ne serez plus assujetis au syndrome de la page blanche. Vous ne partirez pas de zéro mais d'un ensemble déjà construit avec du sens ce qui constitue une base solide. 

Avec cette méthode de prise de notes vous allez pouvoir apprendre tout au long de votre vie un ou plusieurs sujets qu'ils soient reliés ou non. Ce n'est pas juste une méthode pour préparer une thèse ou un examen mais plutôt de sorte à faire correspondre vos idées ensemble.

Le problème de collecter des notes les unes après les autres est de se retrouver perdu dans un vaste système où tout semble indépendant. En revanche, si vous tissez des liens entre différentes idées, vous consolidez vos connaissances et êtes capables de catégoriser les nouvelles idées. Même si vous rangez chaque note ou idée dans une catégorie propre mais qu'il n'y a aucun lien entre, vous ne verrez pas les liaisons qu'il peut y avoir.

Vous avez besoin de 2 choses : (aussi importantes l'une que l'autre) 

- une slip-box (techniquement un endroit où ranger toutes vos notes et idées en respectant une certaine hiérarchie et organisation)
- une habitude à prendre concernant cette nouvelle manière de prise de notes

Pour plus d'infos je (et l'auteur) vous incitent à consulter le livre <a href="https://ilone.hayek.fr/books/getting_things_done/" target=_blank >*Getting Things Done*</a>  de David Allen.  La structure principale de la méthode slip-box repose sur les recommandations de la technique GTD.

Voici donc comment fonctionne cette fameuse slip-box.

Son inventeur se nomme Nicklas Luhmann, il vécut en Allemagne dans les années 60. L'idée principale est de faire correspondre chaque note à une catégorie afin de créer des liens entre différentes notes et d'avoir une vision plus globale d'un sujet. Il remarque qu'une idée prise dans un contexte peut aussi être intéressante et utile dans un autre contexte que celui d'origine. 

Il y a rigoureusement 2 slip-box, une bibliographique et une principale.

La première contient les références bibliographiques de chaque note et un résumé rapide.

La deuxième contient des notes plus développées (max 1 page A4). Ces développements ne comportent pas (ou peu) de citations, il s'agit de reformuler les idées avec ses propres mots, ainsi on s'assure que l'on a bien compris et on retient mieux. Cela doit être réalisé assez rapidement après avoir lu l'article en question (pour des raisons évidentes de mémoire).

La meilleure manière de prendre cette habitude est de lire avec un stylo et une feuille de papier.

Prendre des notes manuscrites est plus bénéfique que des notes dactylographiées car on se restreint à l'essentiel et on retient mieux.

Voici les 3 étapes de création de vos notes :

1. Ayez du brouillon ou un endroit où vous pouvez prendre des notes rapidement et facilement sans avoir à rentrer dans des dossiers ou autres. Cela vous permet de rester focalisés sur le sujet et d'éviter de perdre le fil de vos idées.
2. Etoffez ces notes rapides lorsque vous avez plus de temps ou bien pendant la lecture (cela fonctionne aussi si vous écoutez ou regardez un contenu). Utilisez vos propres mots et allez à l'essentiel, demandez-vous ce que vous souhaitez retenir et ce que vous pourriez réutiliser par la suite. Ces notes permanentes doivent contenir un contexte et êtres comprises même si elles sont relues plus tard. Si ce n'est pas le cas, votre note n'est pas valide. 
3. Développez les notes de l'étape 2 en relation avec vos recherches, vos idées et vos intérêts. Faites des phrases complètes, comme si vous souhaitiez les publier. Ne vous éparpillez pas dans vos pensées, gardez une idée par note. Cette nouvelle idée conforte-t-elle les anciennes ? Ou au contraire, les remet-elle en question ? Quelles sont les ouvertures apportées par la nouvelle note ? En vous posant ces quelques questions vous élaborez des liens entre différentes idées et vous améliorez votre connaissance dans son ensemble. 

Au terme de ces 3 étapes vous pouvez alors jeter les notes de l'étape 1 et vous avez rempli votre slip-box avec les notes de l'étape 2 qui ont été développées et reliées. 

Dans la slip-box, les notes sont triées de manière à ce que les idées similaires soient regroupées, les références sont ajoutées à chaque note et elles sont indexées de manière à les retrouver facilement. 

Vous êtes maintenant capables de faire correspondre des idées entre elles et vous pouvez continuer vos recherches. A partir de ces notes vous allez pouvoir écrire un article, un livre, une thèse ou tout autre sujet écrit.

Encore une fois, gardez les choses le plus simple possible. Le but est de vous faire travailler efficacement, peu importe les outils que vous utilisez. En revanche, vous devez maîtriser les outils que vous utilisez, qu'ils soient puissants ou non. 

Se concentrer sur une tâche précise est la manière la plus efficace. Si vous vous éparpillez dans plusieurs choses à la fois, vous ne trouverez jamais l'état de concentration qui vous permet d'avancer. Reduisez aussi vos possibilités de choix, vous perdrez moins de temps à délibérer entre plusieurs issues (anecdote de Bill Gates ou de Barack Obama qui n'ont que deux costumes un gris et un bleu)

Avec la nouvelle méthode, la question n'est plus "Dans quel sujet dois-je ranger cette note ?" mais plutôt "Dans quel contexte suis-je susceptible de retomber dessus par la suite ?"

L'auteur conseille aussi ne pas faire de brainstorming. Selon lui, cela ne sert à rien car toutes nos idées viennent d'ailleurs, c'est-à-dire "de nos lectures, de nos discussions et de nos interactions avec les autres." 

Le fait de réécrire par nous-mêmes les idées d'un texte en reformulant, permet de vérifier notre compréhension et d'ancrer cela dans notre esprit. La lecture est ainsi active au lieu d'être uniquement passive. De plus, relire plusieurs fois un texte ne permet pas de mieux comprendre. Au contraire, plus on lit plus on devient familier avec et on se persuade que l'on a compris. La seule véritable manière de vérifier notre compréhension est de réécrire avec nos mots. Voilà pourquoi il faudrait toujours lire avec un stylo et un brouillon à portée de main. N'avez-vous jamais remarqué qu'il est beaucoup plus facile de retenir quelque chose que vous avez compris plutôt que d'apprendre par coeur un truc qui n'a pas de sens pour vous ? 

Enfin, n'oubliez pas de relire vos écrits afin de déceler des erreurs de compréhension ou de raisonnement.