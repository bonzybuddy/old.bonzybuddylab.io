---
title: "The Revenant"
date: 2021-02-03T21:26:05+01:00
draft: false
author: Alejandro González Iñárritu
genres: 
- Aventure
principal_actors: "Leonardo di Caprio"
release_year: "2016"
cover:
    img: /covers/the-revenant-cover.jpg
    source: https://www.allocine.fr/film/fichefilm_gen_cfilm=182266.html
---

## 🗣 De quoi ça parle (Spoiler)

L'histoire se déroule en Alaska, où un trappeur est chargé de guider d'autres trappeurs à travers le territoire indigène afin de rejoindre leur camp de base avec des peaux de grizzlis pour pouvoir les revendre.

Ils perdent beaucoup d'hommes après une attaque des indigènes qui évidemment de voient pas d'un bon oeil la présence de trappeurs. Ils décident alors de cacher les peaux et de prendre un raccourci afin de retourner à leur camp et de revenir chercher les peaux avec du renfort.

Malheureusement, le personnage principal (Di Caprio) se fait sauvagement attaquer par une femelle grizzli alors qu'il tentait de les chasser. Récupéré par ses compagnons mais constituant un fardeau pour le reste de la troupe malgré sa connaissance du territoire et sûrs de sa perte imminente, il est laissé pour mort par les hommes qui étaient chargés de l'enterrer. Son fils (qui fait partie de la troupe de trappeur et qui est mi-indigène, mi-Blanc) est tué sous ses yeux par l'autre homme chargé de l'enterrer après sa mort. En raison de ses blessures, il est complétement impuissant face à la scène. Finalement, il ne meurt pas et part à la recherche de son compagnon qui a assassiné son fils. S'ensuit alors une longue quête de survie à travers les forêts glacées du grand Nord, seul, livré à lui-même, mangeant des racines, des herbes et échappant à la mort de nombreuses fois.

Il parvient finalement au campement et est considéré comme un revenant.

Mais l'homme qui l'a abandonné et tué son fils sait pour quelle raison il est revenu et voit sa propre mort approcher à grand pas s'il reste au campement. Après avoir volé l'argent du chef de base, il déserte afin de sauver sa peau. Le chef de base se met en route avec le trappeur afin de le rattraper et de le juger. La traque dure plusieurs jours et l'autre est malin. Il réussit à tuer le général et blesse le trappeur. A la suite d'un long corps à corps dans la neige ce dernier vient à bout du meurtrier et prend ainsi sa vengeance.

## 🔎 Comment je l'ai découvert

C'est un film connu en raison de l'Oscar de Leonardo Di Caprio.

## 💭 Ce que j'en ai pensé

Dans ce monde —car c'est presque un monde parallèle à celui dans lequel nous vivons— les hommes sont devenus des bêtes au même titre que celles qu'ils chassent.

## 👏 Ce que j'ai aimé

J'ai beaucoup aimé les paysages, le film est entièrement tourné à la lumière naturelle de cette région du monde, les longs moments de silence et le message que le film porte, celui de la survie et de la ténacité, de la persévérance d'un homme que tous ont abandonné et qui n'a plus rien pou se rattacher pas même de fils ou de femme encore moins d'argent ou de gloire.

Il m'a donné des idées de photos de paysages et l'envie de visiter cet endroit.

## 👎 Ce que je n'ai pas aimé

Des scènes assez violentes, l'omniprésence du sang. Des combats souvent longs et trop bien détaillés qui montrent pourtant la cruauté des hommes envers d'autres hommes (Blancs contre Indigènes).

## 🎯 Qui l'apprécierait

Ceux qui aiment la nature sauvage et les films avec peu de dialogue.

Ceux qui aiment les combats longs et sanglants.

Ceux qui prévoient de survivre dans un environnement hostile comme le grand nord canadien.

## 📽 Films en relation