---
title: Bienvenue sur mon blog !
---

Vous trouverez sur ce blog toutes mes notes concernant les derniers livres et films que j'ai eu l'occasion de lire ou de voir. Je suis étudiant en école d'ingénieur textile et je souhaite partager mes idées sur différents sujets. Je suis particulièrement intéressé par le son et l'image, la physiologie du sport, la philosophie et la lecture.

Retrouvez également mon podcast <a href="https://anchor.fm/ilonehayek" target="_blank">Agora</a> ainsi que ma <a href="https://www.youtube.com/channel/UCyxzFCnQ7XpdCGrxWxQL5yQ" target="_blank">chaîne YouTube</a>