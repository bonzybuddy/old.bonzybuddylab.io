---
title: "Anything you want"
date: 2021-01-25T23:22:42+01:00
# maj: "08-02-2020"
draft: false
author: "Derek Sivers"
genres:
  - Productivité
  - Life Advice
release_year: "2011"
cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
---

## 🥇🥈🥉 Le livre en 3 phrases

1. Rechercher ce qui nous rend heureux
2. Faire ce que l'on a envie de faire pour son propre plaisir
3. Rendre service aux autres

## 🎣 Impressions

J'ai adoré ce livre ! Le schéma de pensée de l'auteur me correspond tout à fait et je partage ses points de vue.
C'est un livre très court et facile à lire. L'auteur semble être une personne humble malgré son succès et nous délivre de bons conseils.

## 🔎 Comment je l'ai découvert

Une des recommandations d'Ali Abdaal.

## 🎯 Qui devrait le lire

Tous ceux qui cherchent à créer un business pour leur propre plaisir et dans le but d'aider d'autres gens.
Ceux qui n'aspirent pas à diriger une grosse entreprise et qui veulent rester "petit" et indépendants.

## 🎉️ Ce que que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

- J'ai découvert que d'autres personnes raisonnaient comme moi et qu'en plus elles avaient réussi à monter une affaire très fructueuse et enrichissante tout en gardant des valeurs humaines.
  Cela me motive encore plus à continuer dans cette direction, c'est-à-dire faire ce qui me plaît, apprendre par moi-même et résoudre des problèmes auxquels n'importe qui peut faire face.

- Je souhaite rencontrer plus de personnes de ce type et m'associer avec eux.

## 💬 Mes 3 citations préférées

> Starting with no money is an advantage. You don't need money to start helping people.

> Never allow paid placement. (Because it's not fair for who can't afford it.)

> When you want to learn how to do something yourself, most people won't understand. They'll assume the only reason we do anything is to get it done, and doing it yourself is not the most efficient way.

## 📝 Résumé + notes personnelles

Le livre retrace le parcours de Derek Sivers et de son site web permettant aux musiciens indépendants de vendre leurs CD (www.cdbaby.com).

L'histoire est divisée en 40 petits chapitres qui relatent la création et le développement de son projet tout en donnant son avis sur la manière de procéder.

Le livre s'ouvre sur une liste des principes que l'auteur considère comme essentiels et intrinsèques à tout projet :

- Votre business n'est pas une question d'argent, le but est de réaliser votre rêve pour vous et pour les autres.

- Lorsque vous créez une entreprise vous créez une utopie, vous pouvez donc y créer votre monde parfait.

- Ne jamais faire quelque chose uniquement pour l'argent.

- Le succès arrive lorsque vous persistez et cherchez à vous améliorer, pas lorsque vous promettez une solution fictive.

- Votre projet d'entreprise est floue au début. Avant de commencer, vous ne connaissez pas forcément les attentes des gens.

- Débutez sans argent est un avantage, vous n'avez pas besoin d'argent pour commencer à aider les autres.

- Vous ne pouvez pas satisfaire tout le monde.

- Automatisez votre entreprise après son succès, mettez en place des moyens de laisser les choses se dérouler toutes seules après l'envol de votre projet.

- Le but de tout ceci est de faire quelque chose qui vous rende heureux, donc ne créez que ce qui vous satisfait réellement.

Une des premières idées est l'absence de regret. Je suis entièrement d'accord avec le principe de ne jamais regretter quelque chose que l'on a fait ou bien que l'on aurait voulu faire mais qui ne s'est finalement pas réalisée.

Si ce qui vous motive est d'aider les gens, vous pouvez commencer dès à présent sans vous soucier du budget car généralement aucun budget n'est nécessaire pour débuter. L'argent viendra avec la réussite de votre projet, à ce moment vous pourrez développer la forme mais le plus important est le fond de votre projet, c'est-à-dire proposer quelque chose d'utile pour les autres. Avant de créer assurez-vous que cela vous plaise, ne débutez pas un projet avec pour seul but de gagner de l'argent. Prendre du plaisir à créer est essentiel.

Ne jamais autoriser des placements de produits. Ne jamais autoriser de la publicité sous quelconque forme. Le but de votre affaire n'est pas de faire de l'argent mais de rendre service. En incluant des publicités indésirables, vous gâchez l'expérience de l'utilisateur.

Je suis entièrement d'accord avec ce principe. La publicité est quelque chose que je ne tolère pas dans le partage et dans la démarche d'aider les autres. La publicité ne fait pas partie du projet, c'est un parasite qui interfère avec la création. Vous devriez considérer que le but n'est pas de faire du profit mais bien de proposer un service.

Dans le chapitre "If it's not a hit, switch", Derek conseille de persister dans son travail comme de nombreux autres auteurs et entrepreneurs. En revanche, ce qui est vraiment important est de percevoir les signes du succès ou de l'échec. Si l'idée ne rencontre que peu d'engouement auprès du public c'est qu'elle n'est pas la bonne. Il faut persister dans le fait d'innover et de proposer des choses mais il faut aussi savoir changer d'idée ou de thème afin de ne pas rester bloqué sur une seule idée qui ne décolle pas. Cet argument, bien que valable, peut être nuancé. En admettant que le projet est un peu trop en avance sur son temps, il est possible qu'il ne fonctionne pas à son lancement mais qu'il revienne au goût du jour après une évolution du monde ou du marché. Il faut donc être à l'affût et être prêt à rebasculer sur ce projet si une occasion se présente par la suite.

Derek ne cherche pas à compliquer les choses en rentrant dans le monde de la Bourse, de l'économie et des termes compliqués réservés à une poignée élitiste. Pas d'investissement, pas d'actionnaires, pas de côte en Bourse. Il conseille même de développer soi-même son site web plutôt que de faire appel à un développeur afin d'éviter de perdre de l'argent et d'en profiter pour renforcer ses connaissances en apprenant à programmer.

Bien sûr si vous êtes allergique à la programmation il ne faut pas forcer les choses mais essayez tout de même de vous y intéresser un minimum avant de baisser les bras ! Rappelez-vous que vous n'avez pas de budget au commencement, mais rien ne vous empêchera de faire appel à un professionnel par la suite, surtout si vous disposez des moyens financiers.

L'auteur revient souvent sur le fait de placer les clients au coeur des décisions et des enjeux. Tout le projet doit se baser sur aider les clients afin de les rendre heureux à leur tour. Lorsque vous voulez prendre des décisions, n'hésitez pas à leur demander leur avis car votre décision aura sûrement un impact sur eux. Cette approche peut sembler à l'opposé des coutumes mais il ne faut pas oublier que le premier intérêt de votre business repose sur le fait d'apporter une solution à des gens, tout comme vous pourriez en avoir besoin dans un autre domaine. En rendant service aux personnes qui se retrouvent confrontées à un problème que vous avez eu par le passé, vous les rendez heureux d'avoir trouvé une solution et vous vous rendez heureux de les avoir aidés.

Une idée aussi bonne soit elle ne représente pas grand chose tant qu'elle n'est pas mise en oeuvre. Le seul moyen de se rendre compte de son impact de la réaliser. Si elle est réellement utile et bonne, elle rapportera sans doute beaucoup d'argent. Ainsi, une idée médiocre mais réalisée vaut mieux qu'une idée exceptionnelle à l'état de réflection. Les idées ne sont qu'un facteur multiplicatif selon Derek Sivers. Une idée ne rapporte rien, mais son exécution si.

Il propose de ne pas s'embêter avec les mentions légales et autres politiques de confidentialité car presque personne ne s'en préoccupe.

Effectivement je considère cela comme du temps perdu autant à lire qu'à écrire. Pour autant je pense que la politique de confidentialité d'un site internet est relativement importante dans un monde où toutes nos données peuvent être revendues à notre insu. Pouvoir rassurer (ou du moins informer) les clients sur votre transparence est un gage de qualité non négligeable selon moi. Je tends d'ailleurs à éviter les services ayant des politiques de confidentialité obscures et ambigües.

Votre idée n'est qu'une manière de procéder et de résoudre un problème ; il en existe sûrement d'autres. Ne vous focalisez pas sur une seule façon mais sur toutes celles qui peuvent exister.

Lorsque vous voulez apprendre à faire quelque chose de nouveau, on vous conseillera souvent de faire appel à un professionnel. Ceci est la solution facile et simpliste. Il existe pourtant la solution de le faire soi-même mais celle-ci est souvent delaissée car vous pensez que le résultat ne sera pas à la hauteur ou pas dans les temps. C'est pourtant la meilleure manière d'apprendre et de renforcer vos connaissances. Apprenez, essayez, échouez. Rappelez-vous que le but de votre entreprise est de faire quelque chose qui vous rende heureux. En apprenant à surmonter par vous-mêmes les problèmes, vous en tirerez une satisfaction supplémentaire.

Une fois que votre entreprise prend de l'ampleur et gagne en notoriété, de plus en plus de travail s'impose. Vous n'êtes plus aussi heureux qu'au début. Dès lors que ce moment arrive il faut penser à déléguer une partie des tâches pour ne pas être surmené. Cependant, ne déléguez pas tout le travail sinon vous risquez de perdre toute autorité sur vos employés. Il faut trouver un juste milieu en étant toujours présent pour prendre les décisions importantes.

Le jour où votre business ne vous rend plus heureux car l'ambiance dans l'équipe est devenue mauvaise ou que vous n'arrivez plus à gérer ce qui est devenu une grosse entreprise, n'hésitez pas à franchir le pas de vendre. Même si cette idée vous rebute initialement, car il s'agit de votre entreprise, il ne faut pas qu'elle devienne un poison dans votre vie. En vendant, vous vous libérez d'une contrainte importante et vous pourrez alors vous focalisez sur une nouvelle idée toute fraîche et recommencer de zéro dans le but de retrouver votre sentiment de bonheur initial.

N'oubliez pas les principes fondamentaux du livre : soyez heureux et créez dans un but désintéressé et utile !
